﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Dashboardmaster.master" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="Admin_Dashboard" %>

<%--<%@ Register Assembly="DevExpress.Dashboard.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.DashboardWeb" TagPrefix="dx" %>--%>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Docking/Widgets/DateTimeWidget.ascx" TagName="DateTime" TagPrefix="widget" %>
<%@ Register Src="~/Docking/Widgets/MailWidget.ascx" TagName="Mail" TagPrefix="widget" %>
<%@ Register Src="~/Docking/Widgets/CalendarWidget.ascx" TagName="Calendar" TagPrefix="widget" %>
<%@ Register Src="~/Docking/Widgets/NewsWidget.ascx" TagName="News" TagPrefix="widget" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<%--    <dx:ASPxDashboardViewer ID="ASPxDashboardViewer1" runat="server" DashboardTheme="Light" Font-Bold="true" Height="600px" Width="800px" DashboardSource="~/Controls/Eschool Dashboard.xml"></dx:ASPxDashboardViewer>--%>
  
    <%--<div>
          <link href="/Docking/Widgets/widgets.css" rel="Stylesheet" type="text/css" />
    <h4 class="title">Welcome</h4>

     <script type="text/javascript">
         function ShowWidgetPanel(widgetPanelUID) {
             var panel = dockManager.GetPanelByUID(widgetPanelUID);
             panel.Show();
         }
         function SetWidgetButtonVisible(widgetName, visible) {
             var button = ASPxClientControl.GetControlCollection().GetByName('widgetButton_' + widgetName);
             var currentClass = button.GetMainElement().className;
             var newClass = ASPxClientUtils.Trim(visible ? currentClass.replace('disabled', '') : currentClass + ' disabled');
             button.GetMainElement().className = newClass;
         }
    </script>
    <dx:ASPxDockManager runat="server" ID="ASPxDockManager" ClientInstanceName="dockManager">
        <ClientSideEvents
            PanelShown="function(s, e) { SetWidgetButtonVisible(e.panel.panelUID, false) }"
            PanelCloseUp="function(s, e) { SetWidgetButtonVisible(e.panel.panelUID, true) }"/>
    </dx:ASPxDockManager>
    <dx:ASPxDockPanel Theme="SoftOrange" runat="server" ID="DateTimePanel" PanelUID="DateTime" HeaderText="Date & Time"
        Left="820" Top="220" ClientInstanceName="dateTimePanel" Width="230px">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <widget:DateTime ID="DateTimeWidget" runat="server" />
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxDockPanel>
    
    <dx:ASPxDockPanel Theme="SoftOrange" runat="server" ID="MailPanel" PanelUID="Mail" HeaderText="Mail"
        OwnerZoneUID="LeftZone" VisibleIndex="0" ClientInstanceName="mailPanel">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <widget:Mail runat="server" ID="MailWidget" />
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxDockPanel>
    <dx:ASPxDockPanel Theme="SoftOrange" runat="server" ID="CalendarPanel" PanelUID="Calendar" HeaderText="Calendar"
        Width="280px" OwnerZoneUID="LeftZone" VisibleIndex="1" ClientInstanceName="calendarPanel">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <widget:Calendar runat="server" ID="CalendarWidget" />
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxDockPanel>
    
    <dx:ASPxDockPanel Theme="SoftOrange" runat="server" ID="NewsPanel" PanelUID="News" HeaderText="Announcement"
        Width="400px" AllowResize="true" OwnerZoneUID="RightZone" VisibleIndex="1" ClientInstanceName="newsPanel">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <widget:News runat="server" ID="NewsWidget" />
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxDockPanel>
    <div style="height:70px !important;" class="widgetPanel">
        <asp:Repeater runat="server" ID="repeater1">
            <ItemTemplate>
                <dx:ASPxImage runat="server" ImageUrl='<%# String.Format("~/Docking/Images/Widgets/{0}.png", Container.DataItem)%>' Cursor="pointer"
                    ClientInstanceName='<%# "widgetButton_" + Container.DataItem %>' ToolTip='<%# "Show " + Container.DataItem %>'
                    ClientSideEvents-Click='<%# GetClientButtonClickHandler(Container) %>'>
                </dx:ASPxImage>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <dx:ASPxDockZone runat="server" ID="ASPxDockZone1" ZoneUID="LeftZone" CssClass="leftZone"
        Width="340px" PanelSpacing="3">
    </dx:ASPxDockZone>
    <dx:ASPxDockZone runat="server" ID="ASPxDockZone2" ZoneUID="RightZone" CssClass="rightZone"
        Width="400px" PanelSpacing="3">
    </dx:ASPxDockZone>
    </div>--%>

    </asp:Content>

       
 
  
    
    

       
