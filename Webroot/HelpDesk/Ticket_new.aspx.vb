﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.Web
Imports System.IO
Imports DevExpress.Web.ASPxHtmlEditor
Imports System.Drawing
Imports DevExpress.Web.Internal
Partial Class Webroot_HelpDesk_Ticket_new
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)
    Dim plaintext As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            mod_filldropdowns.FillTicketcategoriesddl(Me.ddlcategory)
            datecreated.Date = Date.Now
            datecreated.Enabled = False
            'Dim due As Date = Date.Now
            'duedate.Date = due.AddDays(5)

            txtcreator.Text = Session("uname").ToString()

            txtcreator.Enabled = False
            Dim ticketid As String = Request.QueryString("edit-id")
            If ticketid <> "" Then
                'fetch records
                Dim rec As GHD5.Ticket = (New cls_tickets).SelectThis(ticketid)
                datecreated.Date = rec.CreateDate
                datecreated.Enabled = False

                txtsummary.Text = rec.Summary
                txtdescription.Html = rec.Details
                txtassignee.Text = rec.Assignee
                txtassignee.Visible = True
                lblassignee.Visible = True
                txtassignee.Enabled = False
                txtcreator.Text = rec.Creator
                ddlcategory.Enabled = False
                'ddlpriority.Enabled = False
                ddlcategory.SelectedItem.Text = rec.Category & vbNullString
                'ddlpriority.SelectedValue = rec.Priority & vbNullString
                'duedate.Date = rec.DueDate
                txtccemail.Text = rec.Field1
                'duedate.Enabled = False
                lbltitle.InnerText = String.Format("Ticket '{0}' Edit", ticketid)

                'bindcomments(ticketid)
                'Panel1.Visible = True
                If rec.Status = "CLOSED" Then
                    Msgbox1.ShowHelp("This Ticket has been closed please Re-open to make an Edit")
                    'btnreply.Enabled = False
                    btnsave.Enabled = False

                End If

            Else
                lbltitle.InnerText = "Create new Support Ticket"



            End If



        End If



    End Sub


    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        If txtsummary.Text = "" Then
            Msgbox1.ShowError("Ticket subject is required")
            Exit Sub
        End If
        If txtdescription.Html = "" Then
            Msgbox1.ShowError("Ticket Description is required")
            Exit Sub
        End If
        If ddlcategory.SelectedValue = "NULL" Then
            Msgbox1.ShowError("Please select a category")
            Exit Sub
        End If
        Using mStream As New MemoryStream()
            txtdescription.Export(HtmlEditorExportFormat.Txt, mStream)
            Dim finaltext As String = System.Text.Encoding.UTF8.GetString(mStream.ToArray())
            plaintext = finaltext
        End Using

        Dim ticketid As String = Request.QueryString("edit-id")
        Dim A As New cls_tickets
        If ticketid <> "" Then
            'update ticket records

            Dim rec As GHD5.Ticket = A.SelectThis(ticketid)
            Dim msgdetails As String = ""
            If rec.Summary <> Trim(Me.txtsummary.Text) Then
                msgdetails = "Changed Summary from" & " " & rec.Summary & " " & " to" & " " & Me.txtsummary.Text
                Call mod_main.InsertActivity("EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)
            End If
            If rec.Details <> Trim(plaintext) Then
                msgdetails = "Changed Description from" & " " & rec.Details & " " & " to" & " " & plaintext
                Call mod_main.InsertActivity("EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)
            End If
            If rec.Category <> Trim(Me.ddlcategory.SelectedItem.Text) Then
                msgdetails = "Changed Category from" & " " & rec.Category & " " & " to" & " " & Me.ddlcategory.SelectedItem.Text
                Call mod_main.InsertActivity("EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If
            'If rec.DueDate <> Trim(Me.duedate.Text) Then
            '    msgdetails = "Changed Due date from" & " " & rec.DueDate & " " & " to" & " " & Me.duedate.Text
            '    Call mod_main.InsertActivity("EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            'End If

            'If rec.Priority <> Trim(Me.ddlpriority.SelectedItem.Text) Then
            '    msgdetails = "Changed Priority from" & " " & rec.Priority & " " & " to" & " " & Me.ddlpriority.SelectedItem.Text
            '    Call mod_main.InsertActivity("EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            'End If
            rec.TicketID = Me.txtTicketsID.Text
            rec.Summary = txtsummary.Text.ToUpper
            rec.Details = plaintext
            rec.Assignee = txtassignee.Text.ToUpper
            'rec.DueDate = duedate.Date
            rec.CreateDate = datecreated.Date
            rec.Creator = txtcreator.Text.ToUpper
            rec.Category = ddlcategory.SelectedItem.Text.ToUpper
            rec.CategoryID = ddlcategory.SelectedItem.Value
            'rec.Priority = ddlpriority.SelectedValue
            rec.Field1 = txtccemail.Text
            rec.SubmittedBy = Session("uname")
            rec.SubmittedOn = DateTime.Now.ToLongDateString
            Dim res As ResponseInfo = A.Update(rec)
            If res.ErrorCode = 0 Then
                If ASPxUploadControl1.PostedFile.FileName <> "" Then

                    For Each file In ASPxUploadControl1.UploadedFiles
                        file.SaveAs(Request.PhysicalApplicationPath + "/Attach/" + file.FileName)
                        Dim bb As String = "/Attach/" + file.FileName
                        Call mod_main.InsertActivity("" & Session("uname") & " Added a Photo", "SERVICEDESK", "TICKET", "", Session("uname"), rec.TicketID, bb.ToString, file.FileName)

                    Next
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Your Ticket has been Updated');window.location='opentickets.aspx';", True)

               
                End If

            Else
                Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If


        Else
            'Create fresh record
            Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)

            _Connection.Open()
            Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'TicketID' "
            Dim adap As SqlDataAdapter
            Dim ds As New DataSet
            adap = New SqlDataAdapter(SqlNo, _Connection)
            adap.Fill(ds, "describe")
            Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)

            _Connection.Close()

            'Get the abbreviation of the selected ticket category so as to use it for the autonumber too
            Dim abbr As GHD5.TicketCategory = (New cls_ticketcategories).SelectThisID(ddlcategory.SelectedValue)
            Dim cat As String = abbr.Remarks

            Dim ID As String = "" & cat & "/" & Date.Now.Month & "/" & Date.Now.Year & "/" & Format(NValue, "00")
            Me.txtTicketsID.Text = ID
            'Add 1 to the next number value
            _Connection.Open()
            Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='TicketID' "
            Dim comm1 As New SqlCommand(sqlNoUpD, _Connection)
            comm1.CommandType = CommandType.Text
            comm1.ExecuteNonQuery()
            _Connection.Close()

            'get uploaded file and save it to path

            'Get the least admin with lowest number of open tickets
            Dim assid As String = ""
            _Connection.Open()
            Dim comm2 As New SqlCommand("GetAssigneeCount", _Connection)
            comm2.CommandType = CommandType.StoredProcedure
            comm2.Parameters.AddWithValue("@CategoryID", ddlcategory.SelectedValue)
            comm2.ExecuteNonQuery()
            Dim dr As SqlDataReader
            dr = comm2.ExecuteReader()
            If dr.Read Then
                assid = dr("ASSIGNEE")

            Else

                'if theres no record get assignee from user categories table randomly
                Dim _Connection2 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)
                _Connection2.Open()
                Dim comm3 As New SqlCommand("SELECT username FROM UserCategories where CategoryID=@CategoryID ORDER BY NEWID()", _Connection2)
                comm3.CommandType = CommandType.Text
                comm3.Parameters.AddWithValue("@CategoryID", ddlcategory.SelectedValue)
                comm3.ExecuteNonQuery()
                Dim dr2 As SqlDataReader
                dr2 = comm3.ExecuteReader()
                If dr2.Read Then
                    assid = dr2("Username")
                Else
                    Msgbox1.ShowHelp("There's no Agent found in this Category. Contact your Administrator")
                    Exit Sub

                End If

            End If
            Dim rec As New GHD5.Ticket

            rec.TicketID = Me.txtTicketsID.Text
            rec.Summary = txtsummary.Text.ToUpper
            rec.Details = plaintext
            rec.Assignee = assid.ToUpper

            rec.DueDate = Date.Now.AddDays(3)
            rec.CreateDate = datecreated.Date
            rec.Creator = txtcreator.Text.ToUpper
            Dim CM As GHD5.User = (New cls_users).SelectThisusername(rec.Creator)
            rec.CreatorEmail = CM.Email
            rec.CategoryID = ddlcategory.SelectedValue
            rec.Category = ddlcategory.SelectedItem.Text.ToUpper
            'rec.Priority = ddlpriority.SelectedValue
            rec.Field1 = txtccemail.Text
            rec.SubmittedBy = Session("uname")
            rec.SubmittedOn = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            rec.Status = "QUEUED"
            rec.UpdatedDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            rec.TransGUID = System.Guid.NewGuid.ToString
            rec.Active = 1
            Dim use As GHD5.User = (New cls_users).SelectThisusername(rec.Assignee)
            rec.AssigneeEmail = use.Email

            Dim res As ResponseInfo = A.Insert(rec)
            If res.ErrorCode = 0 Then

                'save attachment
                If ASPxUploadControl1.PostedFile.FileName <> "" Then

                    For Each file In ASPxUploadControl1.UploadedFiles
                        file.SaveAs(Request.PhysicalApplicationPath + "/Attach/" + file.FileName)
                        Dim bb As String = "/Attach/" + file.FileName
                        Call mod_main.InsertActivity("" & Session("uname") & " Added a Photo", "SERVICEDESK", "TICKET", "", Session("uname"), rec.TicketID, bb.ToString, file.FileName)

                    Next
                End If
                'If FileUpload1.PostedFile.FileName <> "" Then
                '    FileUpload1.SaveAs(Request.PhysicalApplicationPath + "/Attach/" + FileUpload1.FileName)
                '    Image1.Visible = True
                '    Image1.ImageUrl = FileUpload1.FileName
                '    Dim bb As String = "/Attach/" + FileUpload1.FileName
                '    Dim cmt As New GHD5.Comment
                '    Dim C As New cls_comments
                '    cmt.TicketID = ID
                '    cmt.Filename = FileUpload1.FileName
                '    cmt.Filepath = bb.ToString
                '    cmt.Submittedby = Session("uname")
                '    cmt.Submittedon = DateTime.Now
                '    cmt.TransGUID = System.Guid.NewGuid.ToString
                '    Try
                '        Dim resinfo As ResponseInfo = C.Insert(cmt)
                '    Catch ex As Exception
                '        Msgbox1.ShowError(ex.Message)
                '    End Try
                'Else
                '    'do nothing
                'End If

                'convert htmleditor to text

                'save activity

                Dim msgdetails As String = ""
                msgdetails = String.Format("Created the ticket: '{0}'", rec.Summary)
                Call mod_main.InsertActivity("CREATE", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), rec.TicketID)
                'populate html body email
                Dim url As String = "" & ConfigurationManager.AppSettings("pathtoAdminticket") & "?edit-id=" & ID & ""
                Dim body As String = mod_main.PopulateBody(ID, txtcreator.Text.ToUpper, txtsummary.Text.ToUpper, plaintext, "", rec.Assignee, url)
                'get the email address of the Assignee


                If mod_main.SendHtmlFormattedEmail(use.Email, rec.Field1, "New Ticket Alert!", body) = True Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Your Ticket has been created');window.location='Tickets.aspx';", True)
                Else
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Your Ticket has been created but Mail not sent');window.location='Tickets.aspx';", True)


                End If


            Else
                Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

            End If
        End If
    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs)
        Response.Redirect("/Webroot/dashboard.aspx")
    End Sub



    'Protected Sub ddlcategory_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim TK As GHD5.TicketCategory = (New cls_ticketcategories).SelectThisID(ddlcategory.SelectedValue)
    '    mod_filldropdowns.FillAssignees(ddlassignee, TK.CategoryID)
    '    If ddlassignee.Items.Count = 0 Then
    '        Msgbox1.ShowHelp("No Agent for " & ddlcategory.SelectedValue & "")
    '        'ElseIf ddlassignee.Items.Count > 1 Then
    '        '    For Each rec As GHD5.User In ddlassignee.Items
    '        '        Dim rc As List(Of GHD5.Ticket) = (New cls_tickets).SelectAssigneetickets(rec.Username)
    '        '        If rc.Count > 0 Then
    '        '            Dim count As Integer = rc.Count
    '        '        End If
    '        '    Next
    '        Exit Sub

    '    End If
    'End Sub




    'Protected Sub btnreply_Click(sender As Object, e As EventArgs)
    '    Dim ticketid As String = Request.QueryString("edit-id")
    '    Dim tk As GHD5.Ticket = (New cls_tickets).SelectThis(ticketid)
    '    Dim A As New cls_comments
    '    Dim rec As New GHD5.Comment
    '    rec.Message = txtreply.Text
    '    rec.Submittedby = Session("uname")
    '    rec.Submittedon = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
    '    rec.TransGUID = System.Guid.NewGuid.ToString
    '    rec.TicketID = ticketid
    '    Dim res As ResponseInfo = A.Insert(rec)
    '    If res.ErrorCode = 0 Then
    '        Dim msgdetails As String = ""
    '        msgdetails = String.Format("Replied the ticket: '{0}'", rec.TicketID)
    '        Call mod_main.InsertActivity("REPLY", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), rec.TicketID)
    '        Dim url As String = "" & ConfigurationManager.AppSettings("pathtoAdminticket") & "?edit-id=" & rec.TicketID & ""
    '        Dim body As String = mod_main.PopulateBodyreply(ticketid, txtcreator.Text.ToUpper, txtsummary.Text, rec.Message, "", txtassignee.Text, url)
    '        Dim use As GHD5.User = (New cls_users).SelectThisusername(txtassignee.Text)
    '        If mod_main.SendHtmlFormattedEmail(use.Email, tk.Field1, "Ticket Reply!", body) = True Then
    '            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Comment added successfully');window.location='Tickets.aspx';", True)
    '        Else
    '            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Comment added but Mail not sent');window.location='Tickets.aspx';", True)


    '        End If

    '    Else
    '        Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
    '    End If
    'End Sub



    'Private Sub bindcomments(ByVal ticketid As String)

    '    Dim dt As New DataTable()
    '    Dim cmd As SqlCommand = Nothing
    '    Dim adp As SqlDataAdapter = Nothing
    '    Try

    '        cmd = New SqlCommand("SELECT * FROM Comments where ticketid=@id order by submittedon DESC", _Connection)
    '        cmd.Parameters.AddWithValue("@id", ticketid)
    '        cmd.CommandType = CommandType.Text
    '        adp = New SqlDataAdapter(cmd)
    '        adp.Fill(dt)

    '        If dt.Rows.Count > 0 Then
    '            Repeater1.DataSource = dt
    '            Repeater1.DataBind()
    '        Else
    '            Label1.Visible = True
    '            Label1.Text = "No recent activities"

    '        End If
    '    Catch ex As Exception
    '        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)
    '    Finally
    '        _Connection.Close()
    '        cmd.Dispose()
    '        adp = Nothing
    '        dt.Clear()
    '        dt.Dispose()
    '    End Try

    'End Sub

    

    Private Const UploadDirectory As String = "~/Attach/"
    'Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)
    '    ''If e.IsValid Then
    '    ''    e.UploadedFile.SaveAs(MapPath("Attach/" + e.UploadedFile.FileName))
    '    ''End If

    '    'Dim resultExtension As String = Path.GetExtension(e.UploadedFile.FileName)
    '    'Dim resultFileName As String = Path.ChangeExtension(Path.GetRandomFileName(), resultExtension)
    '    'Dim resultFileUrl As String = "~/Attach/" + resultFileName
    '    'Dim resultFilePath As String = Server.MapPath(resultFileUrl)
    '    'e.UploadedFile.SaveAs(resultFilePath)

    '    ''UploadingUtils.RemoveFileWithDelay(resultFileName, resultFilePath, 5)

    '    'Dim name As String = e.UploadedFile.FileName
    '    'Dim url As String = ResolveClientUrl(resultFileUrl)
    '    'Dim sizeInKilobytes As Long = e.UploadedFile.ContentLength / 1024
    '    'Dim sizeText As String = sizeInKilobytes.ToString() & " KB"
    '    'e.CallbackData = name & "|" & url & "|" & sizeText


    'End Sub

    

   
  
    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub
End Class
