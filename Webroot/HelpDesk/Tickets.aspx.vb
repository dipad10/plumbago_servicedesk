﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Webroot_HelpDesk_Tickets
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)

    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("/webroot/helpdesk/Ticket_new.aspx")
    End Sub

   

    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
            count += 1
            Dim ticketno As String = ASPxGridView1.GetSelectedFieldValues("TicketID")(i).ToString
            Dim msgdetails As String = ""
            msgdetails = "Deleted Ticket" & ticketno & ""


            _Connection.Open()
            Dim _cmd As SqlCommand = _Connection.CreateCommand()
            _cmd.CommandType = CommandType.Text
            _cmd.CommandText = "delete from Tickets where TicketID=('" & ticketno & "')"
            _cmd.ExecuteNonQuery()
            _Connection.Close()
            Call mod_main.InsertActivity("DELETE", "SERVICEDESK", "TICKET", msgdetails, Session("uname"))
            'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)

            Msgbox1.Showsuccess("" & count & " Ticket(s) deleted successfully!")



        Next

    End Sub

   
End Class
