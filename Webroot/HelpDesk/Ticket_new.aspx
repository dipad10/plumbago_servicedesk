﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Dashboardmaster.master" AutoEventWireup="false" CodeFile="Ticket_new.aspx.vb" Inherits="Webroot_HelpDesk_Ticket_new" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/users.ascx" TagName="user" TagPrefix="uc2" %>

<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>


       <h2 id="lbltitle" runat="server" class="title margin20"></h2>
      <div data-role="panel" class="panel margin20">
    
   
        <div style="z-index:999;" class="content">
              <h5 class="hint-title">Your Question or Issue</h5>
            <hr />
               <div class="popover marker-on-bottom bg-blue">
                  
                                <p class="text-small align-center fg-white align-justify text-bold">Provide a brief overview of your question or issue in the form of a short sentence.</p>
                        </div>
          <div class="margin20">
              <table width="100%">
                <tr id="tr1" visible="false" runat="server">
                    <td>
                         <p class="text-small text-bold">Date Created: </p>
                    </td>
                     <td>
                        <span>  <dx:ASPxDateEdit ID="datecreated" Visible="false" TimeSectionProperties-Visible="true" CssClass="text-secondary" EditFormatString="dd-MMM-yyyy hh:mm tt" runat="server" EnableTheming="True" Theme="SoftOrange" EditFormat="Custom" Width="170px">
                    <TimeSectionProperties>
                        <TimeEditProperties EditFormatString="hh:mm tt" />
                    </TimeSectionProperties>
                </dx:ASPxDateEdit></span>
                         
                    </td>
                </tr>
                <tr id="tr2" runat="server" visible="false">
                       <td>
                       <p class="text-small text-bold">Creator: </p>
                       </td>
                       <td>
                     <span>  <asp:TextBox ID="txtcreator" Visible="false" Width="170px" CssClass="text-secondary" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>&nbsp;<span data-role="hint"
    data-hint-background="bg-green"
    data-hint-color="fg-white"
    data-hint-mode="2"
    data-hint="Ticket Subject"><asp:TextBox ID="txtTicketsID" CssClass="text-secondary" BorderColor="red" BorderWidth="1" runat="server" Visible="False"></asp:TextBox> </span>
                             

                           </span>
    
                       </td>
                  
                </tr>
                <tr>

                      <td><p class="text-bold text-small">Subject:</p></td>

                        <td>
                              <span data-role="hint"
    data-hint-background="bg-green"
    data-hint-color="fg-white"
    data-hint-mode="2"
    data-hint="Ticket Subject"> <dx:ASPxTextBox ID="txtsummary" CssClass="text-secondary shadow" Height="30px" runat="server"  Width="100%"></dx:ASPxTextBox>
      <%--<asp:TextBox ID="txtsummary" CssClass="text-secondary" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>--%> </span>
                             

                        </td>
                   
                </tr>
                <br />
                <tr>
                       <td>
                           <p class="text-small text-bold">Category: </p>
                       </td>
                       <td>
                             <span data-role="hint"
    data-hint-background="bg-green"
    data-hint-color="fg-white"
    data-hint-mode="2"
    data-hint="Category">
                                 <asp:DropDownList ID="ddlcategory"  CssClass="text-secondary shadow" BorderColor="Orange" Width="170px" runat="server">
                           
                         </asp:DropDownList>
                               </span>
                       </td>
                    
                    <td></td>
                       
                </tr>
                  <tr>
                       <td>
                           <p class="text-small text-bold">Cc Email: </p>
                       </td>
                       <td>
                           <span>  
                               <asp:TextBox ID="txtccemail" Width="170px" CssClass="text-secondary shadow" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>  
                               </span>
                       </td>
                  </tr>
                <tr>
              <td><span class="text-bold text-small">Description:</span></td>
                   <td>
                       <dx:ASPxHtmlEditor ID="txtdescription" CssClass="shadow" Height="300px" runat="server"></dx:ASPxHtmlEditor>
                   </td>
                </tr>
                  
                  <tr>
                    
                       
                       <td>
                           <p runat="server" visible="false" id="lblassignee" class="text-small text-bold">Assignee: </p>
                       </td>
                       <td>
                           <span>
                                  <asp:TextBox ID="txtassignee" Visible="false" Width="170px" CssClass="text-secondary shadow" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>  

                           </span>
                       </td>
                        
                   </tr>

            </table>
          </div>
            
      
       
         <table>
             <tr>
                 <td>
                      <p runat="server" class="text-small text-bold">Add Attachment: </p>
                 </td>
                              <td>
                                  <span>
                                       <dx:ASPxUploadControl ID="ASPxUploadControl1" NullText="Upload file" CssClass="shadow" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" runat="server" UploadMode="Standard">
                               <ValidationSettings AllowedFileExtensions=".txt,.jpg,.jpe,.jpeg,.doc,.png" MaxFileSize="1000000">
                                  
                               </ValidationSettings>
                           </dx:ASPxUploadControl>
                                  </span>
                          
                          
                           
                       </td>
                 <td>
                       <p runat="server" class="text-small text-bold">Leave a comment</p>
                 </td>
                 <td></td>
                  </tr>
             <tr>
                 <td></td>
                 <td>
                      <p class="text-small">
            <dx:ASPxLabel ID="AllowedFileExtensionsLabel" runat="server" Text="Allowed file extensions: .jpg, .jpeg, .gif, .png." Font-Size="8pt">
            </dx:ASPxLabel>
            <br />
            <dx:ASPxLabel ID="MaxFileSizeLabel" runat="server" Text="Maximum file size: 4 MB." Font-Size="8pt">
            </dx:ASPxLabel>
        </p>
                 </td>
             </tr>
         </table>
            

             <%--<asp:Panel ID="Panel1" Visible="false" runat="server">
            <h5 class=" hint-text">Comments</h5>
            <hr />
           
                <asp:Repeater ID="Repeater1" runat="server">
    <ItemTemplate>
        
        <div class="panel">        
                <div class="content bg-white">
                <div class="place-right"><span class="text-small"><%#Eval("SubmittedOn", "{0:MMMM d, yyyy}")%> @ <%#Eval("SubmittedOn", "{0:hh:mm tt}")%></span>
                
                </div>
                <div style="width:40px;" class="place-left"><span class="mif-user mif-2x fg-green"></span></div>
                <div style="margin:0px 60px;">
                    <span class="text-secondary text-bold"><%#Eval("SubmittedBy")%></span> <span class="text-small">added a comment</span>
                    <p>
                        <a href="<%#Eval("Filepath")%>" download="<%#Eval("Filename")%>"><img width="100" height="200" src="<%#Eval("Filepath")%>" /></a>
                    
                    </p>
                    <p class="text-small"><%#Eval("Message")%></p>
                </div>
            </div>
          <hr height="0.6px" />
            
        </div>
    </ItemTemplate>
</asp:Repeater>
                 <asp:Label ID="Label1" runat="server" CssClass="text-secondary notify-text fg-brown" Visible="false" Text=""></asp:Label>
                <asp:TextBox ID="txtreply" Height="30px" TextMode="MultiLine" Width="100%" CssClass="text-secondary" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>
                <p> <asp:Button ID="btnreply" Visible="true" Height="29px" OnClick="btnreply_Click" CssClass="button text-small bg-green fg-white mini-button" runat="server" Text="Comment" Font-Size="X-Small" Font-Bold="True" /></p>
            </asp:Panel>--%>



             </div>
               
          
         <div class="align-center">
                   <span>  &nbsp;
                     <asp:Button ID="btnsave" Visible="true" Height="29px" OnClick="btnsave_Click" CssClass="button shadow text-small bg-green fg-white mini-button" runat="server" Text="Submit Request" Font-Size="X-Small" Font-Bold="True" />
    
                   </span>

                                 <span>  
                             <asp:Button ID="btncancel" Visible="true" Height="29px" OnClick="btncancel_Click" CssClass="button text-small bg-darkRed fg-white mini-button" runat="server" Text="Cancel" Font-Size="X-Small" Font-Bold="True" />
 
                                    </span>

                </div>
</div>

   
</asp:Content>

