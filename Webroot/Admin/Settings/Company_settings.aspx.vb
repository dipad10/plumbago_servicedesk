﻿
Partial Class Webroot_Admin_Settings_Company_settings
    Inherits System.Web.UI.Page

    Protected Sub cmdsave_Click(sender As Object, e As EventArgs)
        Dim u As New cls_settings
        Dim rec As GHD5.Setting = u.SelectThisID(1)
        rec.COMPANY_NAME = txtcompname.Text
        rec.COMPANY_ADDRESS = txtcompaddress.Text
        rec.COMPANY_ABREV = txtcompabrev.Text
        rec.SMTP_SERVER = txtsmtpserver.Text
        rec.SMTP_PORT = txtsmtpport.Text
        rec.SMTP_PASSWORD = txtSmtppwd.Text
        rec.SMTP_EMAIL = txtsmtpemail.Text
        rec.SMTP_USERNAME = txtsmtpusername.Text
        rec.INITIAL_PWD_DAYS = txtinitialpwd.Text
        rec.CHANGE_PWD_DAYS = txtchangepwd.Text
        rec.AD_SERVER = txtadserver.Text
        rec.AD_USERNAME = txtadusername.Text
        rec.AD_PASSWORD = txtadpassword.Text
        rec.SMS_ACCOUNT = txtsmsacct.Text
        rec.SMS_PASSWORD = txtsmspassword.Text
        Dim res As ResponseInfo = u.Update(rec)
        If res.ErrorCode = 0 Then
            Msgbox1.Showsuccess("Data saved successfully")
        Else
            Msgbox1.ShowError("" & res.ErrorMessage & " " & res.ExtraMessage & "")
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim u As New cls_settings
            Dim rec As GHD5.Setting = u.SelectThisID(1)
            txtcompname.Text = rec.COMPANY_NAME
            txtcompaddress.Text = rec.COMPANY_ADDRESS
            txtcompabrev.Text = rec.COMPANY_ABREV
            txtsmtpemail.Text = rec.SMTP_EMAIL
            txtsmtpport.Text = rec.SMTP_PORT
            txtSmtppwd.Text = rec.SMTP_PASSWORD
            txtsmtpserver.Text = rec.SMTP_SERVER
            txtadserver.Text = rec.AD_SERVER
            txtadpassword.Text = rec.AD_PASSWORD
            txtadusername.Text = rec.AD_USERNAME
            txtinitialpwd.Text = rec.INITIAL_PWD_DAYS
            txtchangepwd.Text = rec.CHANGE_PWD_DAYS
            txtsmtpusername.Text = rec.SMTP_USERNAME
            txtsmsacct.Text = rec.SMS_ACCOUNT
            txtsmspassword.Text = rec.SMS_PASSWORD
        End If
    End Sub
End Class
