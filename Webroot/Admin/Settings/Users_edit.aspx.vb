﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Data.SqlClient
Partial Class Webroot_Admin_Settings_Users_edit
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim UserID As String = Request.QueryString("edit-id")
            If UserID <> "" Then
                'Fetch old records into textboxes
                Dim rec As GHD5.User = (New cls_users).SelectThisID(UserID)

                Me.txtuserID.Text = rec.UserID
                Me.txtusername.Text = rec.Username
                Me.txtpassword.Text = mod_main.Decrypt(rec.Password)
                Me.txtfirstname.Text = rec.FirstName
                Me.txtlastname.Text = rec.LastName
                Me.txtemail.Text = rec.Email
                Me.txtaddress.Text = rec.Address
                Me.txtphone.Text = rec.Phone
                Me.chkactive.Checked = rec.Active
                Me.expiry.Date = rec.PwdExpiry & vbNullString
                Me.txtconfirm.Text = mod_main.Decrypt(rec.Password)
                lbluser.InnerHtml = "User " & UserID & " Edit"
                If rec.permission = "Admin" Then
                    Me.chkisadmin.Checked = True
                Else
                    'check basic user checkbox
                    Me.chkuser.Checked = True

                End If
            Else
                lbluser.InnerHtml = "Setup new User"
            End If
        End If

    End Sub

    Protected Sub cmdsave_Click(sender As Object, e As EventArgs)
        If txtpassword.Text <> txtconfirm.Text Then
            Msgbox1.ShowError("The passwords does not match. please check and try again")
            Exit Sub
        End If
        If chkisadmin.Checked = False And chkuser.Checked = False Then
            Msgbox1.ShowError("Please select user Permission")
            Exit Sub
        End If



        Dim UserID As String = Request.QueryString("edit-id")
        Dim U As New cls_users

        'Update old records
        If UserID <> "" Then
            Dim Rec As GHD5.User = U.SelectThisID(UserID)
            Rec.UserID = UserID
            Rec.Username = Me.txtusername.Text
            Rec.Password = mod_main.Encrypt(Me.txtpassword.Text) 'Me.txtpassword.Text
            Rec.FirstName = Me.txtfirstname.Text
            Rec.LastName = Me.txtlastname.Text
            Rec.Address = Me.txtaddress.Text
            Rec.Email = Me.txtemail.Text
            Rec.Phone = Me.txtphone.Text
            Rec.PwdExpiry = Me.expiry.Date
            Rec.SecQuestion = ""
            Rec.SecAnswer = ""
            If Me.chkactive.Checked = True Then
                Rec.Active = "1"
            ElseIf Me.chkdisabled.Checked = True Then
                Rec.Active = "0"

            End If
            If Me.chkisadmin.Checked = True Then
                Rec.permission = "Admin"
            ElseIf Me.chkuser.Checked = True Then
                Rec.permission = "BasicUser"

            End If
            Dim res As ResponseInfo = U.Update(Rec)
            If res.ErrorCode = 0 Then
                Msgbox1.Showsuccess("data saved successfully")
                'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Users.aspx';", True)
            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If
            'End If

        Else

            'Create New Records
            'Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)


            _Connection.Open()
            Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'UserID' "
            Dim adap As SqlDataAdapter
            Dim ds As New DataSet
            adap = New SqlDataAdapter(SqlNo, _Connection)
            adap.Fill(ds, "describe")
            Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)
            _Connection.Close()

            Dim ID As String = "GHD" & "/" & "U" & "/" & Format(NValue, "00")
            Me.txtuserID.Text = ID

            _Connection.Open()
            Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='UserID' "
            Dim comm1 As New SqlCommand(sqlNoUpD, _Connection)
            comm1.CommandType = CommandType.Text
            comm1.ExecuteNonQuery()
            _Connection.Close()

            Dim Rec As New GHD5.User
            Rec.UserID = Me.txtuserID.Text
            Rec.Username = Me.txtusername.Text
            Rec.Password = mod_main.Encrypt(Me.txtpassword.Text) 'Me.txtpassword.Text
            Rec.FirstName = Me.txtfirstname.Text
            Rec.LastName = Me.txtlastname.Text
            Rec.Address = Me.txtaddress.Text
            Rec.Email = Me.txtemail.Text
            Rec.Phone = Me.txtphone.Text
            Rec.PwdExpiry = Me.expiry.Date
            Rec.SecQuestion = ""
            Rec.SecAnswer = ""
            If Me.chkactive.Checked = True Then
                Rec.Active = "1"
            ElseIf Me.chkdisabled.Checked = True Then
                Rec.Active = "0"

            End If
            If Me.chkisadmin.Checked = True Then
                Rec.permission = "Admin"
            ElseIf Me.chkuser.Checked = True Then
                Rec.permission = "BasicUser"
            End If

            Rec.SubmittedBy = Session("uname")
            Rec.SubmittedOn = DateTime.Now.ToLongDateString
            Dim res As ResponseInfo = U.Insert(Rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Users.aspx';", True)
            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If
        End If
    End Sub
End Class
