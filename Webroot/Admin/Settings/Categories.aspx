﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="Categories.aspx.vb" Inherits="Webroot_Admin_Settings_Categories" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
       <h4 id="lblcategory" runat="server" class="title margin20"></h4>
    <div data-role="panel" class="panel margin20">
    
   
        <div style="z-index:999;" class="content">
              <h5 class="hint-text">Category Details</h5>
            <hr />
             <div class="popover marker-on-bottom bg-red">
                            <div class="fg-white text-small text-bold">Setup of Categories and assign an Abrreviation to them, which is shown on each autonumber generated.</div>
                        </div>
            <div>
                <table width="100%">
                <tr height="50px">
                     <td>
                         <p class="text-secondary text-bold">Category ID: </p>
                    </td>
                    <td>
                      <span>  <asp:TextBox ID="txtCategoryid" Enabled="false" Text="AUTO" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>
                    <td> <p class="text-secondary text-bold">Category Name: </p></td>
                    <td>
                       <span>  <asp:TextBox ID="txtcategoryname" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>
                                   </tr>
                    <tr>
                         <td>
                         <p class="text-secondary text-bold">Abbreviation: </p>
                    </td>
                    <td>
                          <span>  <asp:TextBox ID="txtabbreviation" CssClass="text-secondary" BorderColor="Orange" BorderWidth="1" runat="server"></asp:TextBox></span>

                    </td>

                    </tr>
          </table>
               
            </div>
          
         
        <br />
        
               <dx:ASPxGridView ID="ASPxGridView1" Width="100%" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource2" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True" >
                   

<SettingsPager PageSize="7"></SettingsPager>

                   <Settings ShowFilterBar="Auto" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                           <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>
                        <dx:GridViewDataHyperLinkColumn FieldName="CategoryID" PropertiesHyperLinkEdit-Style-CssClass=" text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="CategoryID" PropertiesHyperLinkEdit-NavigateUrlFormatString="Categories_edit.aspx?edit-id={0}" VisibleIndex="1">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="Categories.aspx?edit-id={0}" TextFormatString="CategoryID">
                        </PropertiesHyperLinkEdit>


                       </dx:GridViewDataHyperLinkColumn>
                    
                  
                      <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Categoryname" VisibleIndex="2">
                       </dx:GridViewDataTextColumn>

                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Remarks" VisibleIndex="3">
                       </dx:GridViewDataTextColumn>

                   
                   </Columns>

                  <Styles>
            <AlternatingRow Enabled="true" />
                     
<SelectedRow ForeColor="White"></SelectedRow>
                     
        </Styles>
               </dx:ASPxGridView>
                        

                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:GuruHelpDeskConnectionString %>" SelectCommand="SELECT * FROM [TicketCategories] ORDER BY [SN] DESC"></asp:SqlDataSource>
                        

               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                          <asp:Button ID="cmdsave" OnClick="cmdsave_Click" runat="server" Height="29px" CssClass="button text-small bg-darkMagenta fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="Save" />
                      </td>
                          
                           <td>
                          <asp:Button ID="btncancel" Visible="true" Height="29px" OnClientClick="history.go(-1)" CssClass="button text-small bg-red fg-white mini-button" runat="server" Text="Cancel" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                           <td>
                               <asp:Button ID="btndelete" Visible="true" Height="29px" OnClick="btndelete_Click" CssClass="button bg-lightRed fg-white mini-button" runat="server" Text="Delete" Font-Size="X-Small" Font-Bold="True" />
                           </td>
                          
                       </tr>
                       
                   </table>
               </div>

</div>
    </div>
</asp:Content>

