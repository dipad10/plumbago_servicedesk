﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="UserAllocations.aspx.vb" Inherits="Webroot_Admin_Settings_UserAllocations" %>

<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <h3 class="title fg-blue margin20">Categories Allocation</h3>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
        </ContentTemplate>

    </asp:UpdatePanel>


    <div class="panel margin20">
        <div class="content">
            <h5 id="lblallocate" runat="server" class="hint-text">Assign Categories to Admin Users</h5>
            <hr />


            <br />
            <div class="margin20">
                <p visible="true" class="text-small text-bold">
                    Select Assignee: <span>
                        <dx:ASPxComboBox ID="ddlassignee" Theme="PlasticBlue" runat="server" ValueType="System.String"></dx:ASPxComboBox>
                    </span>
                </p>
            </div>


            <br />
            <asp:Panel ID="panelallocate" runat="server">
                <%--<div class="grid">
                    <div class="row cells4">
                       
                        <div class="cell colspan1">

                            <span class="text-small bg-lightGreen text-bold">List of All categories</span>
                            <br />
                            <asp:ListBox ID="ListBox1" SelectionMode="Multiple" Height="200"
                                Width="300" runat="server"></asp:ListBox>
                        </div>

                        <div class="cell">
                             <p>

                        <asp:Button ID="btnadd" OnClick="btnadd_Click" runat="server" Height="29px" CssClass="button text-small bg-darkBlue fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="Add selected items" />

                    </p>

                    <p>
                        <asp:Button ID="btnsave" OnClick="btnsave_Click" runat="server" Height="29px" CssClass="button text-small bg-darkBlue fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="Save" />
                    </p>

                        </div>

                        <div class="cell colspan2">
                            <span class="text-small bg-lightGreen text-bold">Categories assigned to AdminUser</span>
                            <br />
                             
                            <asp:ListBox ID="ListBox2" Height="200"
                                Width="300" runat="server"></asp:ListBox>
                        </div>
                       
                    </div>
                    <br />
                   

                </div>--%>
                <table class="margin20" width="100%">
                    <tr>

                        <td>
                            <span class="text-small bg-lightGreen text-bold">List of All categories</span>
                            <br />
                            <asp:ListBox ID="ListBox1" SelectionMode="Multiple" Height="200"
                                Width="300" runat="server"></asp:ListBox>
                        </td>
                        <td>
                            <p>

                                <asp:Button ID="btnadd" OnClick="btnadd_Click" runat="server" Height="29px" CssClass="button text-small bg-darkBlue fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="Add selected items" />

                            </p>
                             <p>

                                <asp:Button ID="btnremove" OnClick="btnremove_Click" runat="server" Height="29px" CssClass="button text-small bg-darkBlue fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="Remove selected items" />

                            </p>


                        </td>
                        <td></td>
                        <td><span class="text-small bg-lightGreen text-bold">Categories assigned to AdminUser</span>
                            <br />

                            <asp:ListBox ID="ListBox2" Height="200"
                                Width="300" runat="server"></asp:ListBox>
                        </td>
                    </tr>
                </table>
                <div class="align-center">
                    <table>
                        <tr>
                            <td>
                                <p>
                                    <asp:Button ID="btnsave" OnClick="btnsave_Click" runat="server" Height="29px" CssClass="button text-small bg-darkBlue fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="Save" />
                                </p>
                            </td>



                            <td>
                                <asp:Button ID="btncancel" Visible="true" Height="29px" OnClientClick="history.go(-1)" CssClass="button text-small bg-red fg-white mini-button" runat="server" Text="Cancel" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                            </td>


                        </tr>

                    </table>
                </div>
            </asp:Panel>

        </div>
    </div>
</asp:Content>

