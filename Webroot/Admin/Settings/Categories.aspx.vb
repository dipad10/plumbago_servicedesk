﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Webroot_Admin_Settings_Categories
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim CategoryID As String = Request.QueryString("edit-id")
            If CategoryID <> "" Then
                'fetch old records into text boxes
                Dim rec As GHD5.TicketCategory = (New cls_ticketcategories).SelectThisID(CategoryID)

                Me.txtCategoryid.Text = rec.CategoryID
                Me.txtcategoryname.Text = rec.Categoryname
                Me.txtabbreviation.Text = rec.Remarks

            End If
        End If
    End Sub
   
    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
            count += 1
            Dim catID As String = ASPxGridView1.GetSelectedFieldValues("CategoryID")(i).ToString
            Dim catname As String = ASPxGridView1.GetSelectedFieldValues("Categoryname")(i).ToString
            Dim msgdetails As String = ""
            msgdetails = "Deleted Category" & catname & ""


            _Connection.Open()
            Dim _cmd As SqlCommand = _Connection.CreateCommand()
            _cmd.CommandType = CommandType.Text
            _cmd.CommandText = "delete from TicketCategories where CategoryID=('" & catID & "')"
            _cmd.ExecuteNonQuery()
            _Connection.Close()
            Call mod_main.InsertActivity("DELETE", "SERVICEDESK", "CATEGORY", msgdetails, Session("uname"))
            'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)

            Msgbox1.Showsuccess("" & count & " Categories(s) deleted successfully!")



        Next

    End Sub

    Protected Sub cmdsave_Click(sender As Object, e As EventArgs)
        Dim CategoryID As String = Request.QueryString("edit-id")
        Dim C As New cls_ticketcategories

        If CategoryID <> "" Then
            'update old records
            Dim rec As GHD5.TicketCategory = C.SelectThisID(CategoryID)

            rec.CategoryID = Me.txtCategoryid.Text
            rec.Categoryname = Me.txtcategoryname.Text
            rec.Remarks = Me.txtabbreviation.Text
            Dim res As ResponseInfo = C.Update(rec)
            If res.ErrorCode = 0 Then
                Msgbox1.Showsuccess("data saved successfully")
            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)
            End If

        Else

            _Connection.Open()
            Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'CategoryID' "
            Dim adap As SqlDataAdapter
            Dim ds As New DataSet
            adap = New SqlDataAdapter(SqlNo, _Connection)
            adap.Fill(ds, "describe")
            Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)
            _Connection.Close()

            Dim ID As String = "GHD" & "/" & "CAT" & "/" & Format(NValue, "00")
            Me.txtCategoryid.Text = ID

            _Connection.Open()
            Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='CategoryID' "
            Dim comm1 As New SqlCommand(sqlNoUpD, _Connection)
            comm1.CommandType = CommandType.Text
            comm1.ExecuteNonQuery()
            _Connection.Close()

            Dim rec As New GHD5.TicketCategory
            rec.CategoryID = Me.txtCategoryid.Text
            rec.Categoryname = Me.txtcategoryname.Text
            rec.Remarks = Me.txtabbreviation.Text

            Dim res As ResponseInfo = C.Insert(rec)
            If res.ErrorCode = 0 Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Categories.aspx';", True)
            Else
                Me.Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

            End If
        End If

    End Sub

End Class
