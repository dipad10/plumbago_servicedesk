﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="Users.aspx.vb" Inherits="Webroot_Admin_Settings_Users" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
               <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" ConfirmText="Are you sure you want to delete this User" TargetControlID="btndelete" runat="server" />


    <div>
         <h4 class="title fg-blue margin20">All Users</h4>
        <div class="panel margin20">
           <div class="content">
                <h5 class="hint-title">All Users</h5>
            <hr />
               <div class="popover marker-on-bottom bg-red">
                            <div class="fg-white text-small text-bold">Fill In the User details and choose permissions either Admin or Basic User</div>
                        </div>
              <dx:ASPxGridView ID="ASPxGridView1" Width="100%" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False"  DataSourceID="SqlDataSource1" KeyFieldName="UserID" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True" >
                   

<SettingsPager PageSize="7"></SettingsPager>

                   <Settings ShowFilterBar="Auto" />
                   <SettingsSearchPanel Visible="True" />
                   <Columns>
                    
                     <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                           
                       </dx:GridViewCommandColumn>

                              <dx:GridViewDataHyperLinkColumn FieldName="UserID" PropertiesHyperLinkEdit-Style-CssClass=" text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="UserID" PropertiesHyperLinkEdit-NavigateUrlFormatString="Users_edit.aspx?edit-id={0}" VisibleIndex="1">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="Users_edit.aspx?edit-id={0}" TextFormatString="UserID">
                        </PropertiesHyperLinkEdit>
                       </dx:GridViewDataHyperLinkColumn>
                    
                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Username" VisibleIndex="1">
                       </dx:GridViewDataTextColumn>

                   
                           <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="FirstName" VisibleIndex="2">
                           </dx:GridViewDataTextColumn>
                            
                           <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="LastName" VisibleIndex="3">
                           </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Email" VisibleIndex="4">
                           </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Phone" VisibleIndex="5">
                           </dx:GridViewDataTextColumn>

                            <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small" FieldName="LastLoginDate" VisibleIndex="6">
                       </dx:GridViewDataDateColumn>

                           <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="SubmittedBy" VisibleIndex="7">
                           </dx:GridViewDataTextColumn>
                             <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small" FieldName="SubmittedOn" VisibleIndex="6">
                       </dx:GridViewDataDateColumn>
                   
                          
                   </Columns>

                  <Styles>
            <AlternatingRow Enabled="true" />
                     
<SelectedRow ForeColor="White"></SelectedRow>
                     
        </Styles>
               </dx:ASPxGridView>
                        

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:GuruHelpDeskConnectionString %>" SelectCommand="SELECT [UserID], [Username], [FirstName], [LastName], [Email], [Phone], [LastLoginDate], [SubmittedBy], [SubmittedOn] FROM [Users] ORDER BY [SubmittedOn] DESC"></asp:SqlDataSource>
                        

                        

               <div class="align-center">
                   <table>
                       <tr>
                      <td>
                          <asp:Button ID="btnnew" OnClick="btnnew_Click" runat="server" Height="29px" CssClass="button text-small bg-darkMagenta fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="New User" />
                      </td>
                          
                           <td>
                          <asp:Button ID="btncancel" Visible="true" Height="29px" OnClientClick="history.go(-1)" CssClass="button text-small bg-red fg-white mini-button" runat="server" Text="Cancel" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                           </td>
                           <td>
                               <asp:Button ID="btndelete" Visible="true" Height="29px" OnClick="btndelete_Click" CssClass="button bg-lightRed fg-white mini-button" runat="server" Text="Delete" Font-Size="X-Small" Font-Bold="True" />
                           </td>
                          
                       </tr>
                       
                   </table>
               </div>
           </div>
       </div>
    </div>

</asp:Content>

