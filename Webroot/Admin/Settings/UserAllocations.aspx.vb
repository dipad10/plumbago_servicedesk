﻿

Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Webroot_Admin_Settings_UserAllocations
    Inherits System.Web.UI.Page
    Dim Conn As New SqlConnection(m_strconnString)
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim UserID As String = Request.QueryString("edit-id")
            If UserID <> "" Then
                Dim rec As GHD5.User = (New cls_users).SelectThisID(UserID)
                ddlassignee.Value = rec.Username
                Dim recs As List(Of GHD5.UserCategory) = (New Cls_Usercategories).SelectThisuserid(UserID)
                ListBox2.DataSource = recs
                ListBox2.DataTextField = "Categoryname"
                ListBox2.DataValueField = "CategoryID"
                ListBox2.DataBind()
            End If
            bindcategories()
            FillAssignees(ddlassignee)
            'With Me.ListBox2
            '    .DataSource = (New Cls_Usercategories).SelectThisuserid(UserID)
            '    .DataTextField = "categoryname"
            '    .DataValueField = "categoryID"
            '    .DataBind()
            'End With
        End If
    End Sub
    Protected Sub btnadd_Click(sender As Object, e As EventArgs)
       
            For Each item As ListItem In ListBox1.Items
                If item.Selected And Not ListBox2.Items.Contains(item) Then
                    Dim newItem As New ListItem(item.Text, item.Value)
                    ListBox2.Items.Add(newItem)
              
                End If

            Next

    End Sub

    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        Dim userid As String = Request.QueryString("edit-id")
        If userid <> "" Then
            Dim rec As GHD5.User = (New cls_users).SelectThisID(userid)
            Dim AR As New Cls_Usercategories
            AR.RemoveAllusercategories(userid)
            For Each i As ListItem In Me.ListBox2.Items
                AR.Insertusercategories(userid, i.Value, i.ToString, rec.Username, Session("uname"))
            Next
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Allocations.aspx';", True)
        Else

            Dim rec As GHD5.User = (New cls_users).SelectThisusername(ddlassignee.Value)
            Dim AR As New Cls_Usercategories
            AR.RemoveAllusercategories(userid)
            For Each i As ListItem In Me.ListBox2.Items
                AR.Insertusercategories(rec.UserID, i.Value, i.ToString, rec.Username, Session("uname"))
            Next
            ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Allocations.aspx';", True)
        End If
       
        '      Dim count As Integer = 0
        'Dim connect5 As New SqlConnection(ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)

        'connect5.Open()

        'For i As Integer = 0 To ListBox2.Items.Count - 1
        '    If ListBox2.Items(i).Selected = False Then
        '        count += 1
        '        For Each Items As String In ListBox2.Items(i).ToString
        '            Dim rec As GHD5.TicketCategory = (New cls_ticketcategories).SelectThiscategoryname(Items)
        '            Dim _cmd5 As SqlCommand = connect5.CreateCommand()
        '            _cmd5.CommandType = CommandType.Text
        '            _cmd5.CommandText = "INSERT INTO UserCategories ([CategoryID], [UserID], [Username]) VALUES (@categoryid, @userid, @username)"
        '            '_cmd5.Parameters.AddWithValue("@categoryid", ListBox2.Items(i).ToString())
        '            _cmd5.Parameters.AddWithValue("@userid", ddlassignee.Text)
        '            _cmd5.Parameters.AddWithValue("@username", ddlassignee.Value)
        '            _cmd5.Parameters.AddWithValue("@categoryid", rec.CategoryID)

        '            _cmd5.ExecuteNonQuery()
        '        Next


        '    End If

        'Next

    End Sub

    Private Sub bindcategories()
        Dim recs As List(Of GHD5.TicketCategory) = (New cls_ticketcategories).SelectAllcategories
        ListBox1.DataSource = recs
        ListBox1.DataTextField = "Categoryname"
        ListBox1.DataValueField = "CategoryID"
        ListBox1.DataBind()
    End Sub

    'Private Sub bindcategories2()
    '    Dim recs As List(Of GHD5.TicketCategory) = (New cls_ticketcategories).SelectAllcategories
    '    ListBox2.DataSource = recs
    '    ListBox1.DataTextField = "Categoryname"
    '    ListBox1.DataValueField = "CategoryID"
    '    ListBox1.DataBind()
    'End Sub

    Protected Sub btnremove_Click(sender As Object, e As EventArgs)
        For Each item As ListItem In ListBox2.Items
            If item.Selected And Not ListBox1.Items.Contains(item) Then
                Dim newItem As New ListItem(item.Text, item.Value)
                ListBox1.Items.Add(newItem)
                'ListBox2.Items.Remove(item)
            End If

        Next
    End Sub
End Class
