﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="Allocations.aspx.vb" Inherits="Webroot_Admin_Settings_Allocations" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
        </ContentTemplate>

    </asp:UpdatePanel>
    <h4 id="lblcategory" runat="server" class="title margin20">Categories Allocation</h4>
    <div data-role="panel" class="panel margin20">


        <div style="z-index: 999;" class="content">
            <h5 class="hint-text">All Allocations</h5>
            <hr />
            <div class="popover marker-on-bottom bg-red">
                <div class="fg-white text-small text-bold">To allocate a category to an adminuser, click "new" button below the Grid.</div>
            </div>

            <dx:ASPxGridView ID="ASPxGridView1" Width="100%" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True">


                <SettingsPager PageSize="7"></SettingsPager>

                <Settings ShowFilterBar="Auto" />
                <SettingsSearchPanel Visible="True" />
                <Columns>
                    <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataHyperLinkColumn FieldName="UserID" PropertiesHyperLinkEdit-Style-CssClass=" text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="UserID" PropertiesHyperLinkEdit-NavigateUrlFormatString="UserAllocations.aspx?edit-id={0}" VisibleIndex="1">
                        <PropertiesHyperLinkEdit NavigateUrlFormatString="UserAllocations.aspx?edit-id={0}" TextFormatString="UserID">
                        </PropertiesHyperLinkEdit>


                    </dx:GridViewDataHyperLinkColumn>


                       <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Categoryname" VisibleIndex="2">
                       </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Username" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>


                </Columns>

                <Styles>
                    <AlternatingRow Enabled="true" />

                    <SelectedRow ForeColor="White"></SelectedRow>

                </Styles>
            </dx:ASPxGridView>


            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:GuruHelpDeskConnectionString %>" SelectCommand="SELECT * FROM [UserCategories] ORDER BY [SN] DESC"></asp:SqlDataSource>


            <div class="align-center">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnnew" OnClick="btnew_Click" runat="server" Height="29px" CssClass="button text-small bg-green fg-white mini-button" Font-Size="X-Small" Font-Bold="True" Text="New Allocation" />
                        </td>

                        <td>
                            <asp:Button ID="btncancel" Visible="true" Height="29px" OnClientClick="history.go(-1)" CssClass="button text-small bg-red fg-white mini-button" runat="server" Text="Cancel" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                        </td>


                    </tr>

                </table>
            </div>
        </div>

    </div>

</asp:Content>

