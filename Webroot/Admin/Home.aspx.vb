﻿
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Partial Class Webroot_Admin_Home
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim constr As String = ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString
            Using con As New SqlConnection(constr)
                con.Open()

                Using cmd As New SqlCommand()
                    cmd.CommandText = "select count (*) from tickets where DueDate <=@duedate and Active='1' and assignee=@assignee"
                    cmd.Connection = con
                    cmd.Parameters.AddWithValue("@duedate", DateTime.Now)
                    cmd.Parameters.AddWithValue("@assignee", Session("uname"))
                    Dim count As Int32 = DirectCast(cmd.ExecuteScalar(), Int32)
                    If count = 0 Then
                        Exit Sub
                    Else
                        'Response.Write("<script>alert('You have " & count & " Tickets Due! Please Attend to them.');</script>")
                        Msgbox1.ShowHelp("You have " & count & " Tickets Due! Please Attend to them.")

                    End If


                End Using
            End Using


        End If
    End Sub
End Class
