﻿Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.Web

Partial Class Webroot_Admin_HelpDesk_Ticket_new
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            mod_filldropdowns.FillTicketcategoriesddl(Me.ddlcategory)
            mod_filldropdowns.Fillcalltypes(Me.ddlcalltype)
            mod_filldropdowns.FillAssignees(Me.ddlassignee)
            Dim ticketid As String = Request.QueryString("edit-id")
            If ticketid <> "" Then
                'fetch records
                Dim rec As GHD5.Ticket = (New cls_tickets).SelectThis(ticketid)

                txtsummary.Text = rec.Summary
                txtdescription.Text = rec.Details
                ddlassignee.Text = rec.Assignee

                combo1.SelectedText = rec.Creator

                ddlcategory.SelectedValue = rec.CategoryID & vbNullString

                txtccemail.Text = rec.Field1
                bindtime(rec.UpdatedDate)

                'lbltimespent.InnerHtml =
                ddlcalltype.SelectedValue = rec.CallType & vbNullString
                ddlpriority.SelectedValue = rec.Priority
                ddlstatus.SelectedValue = rec.Status
                duedate.Date = rec.DueDate & vbNullString

                bindcomments(ticketid)
                Panel1.Visible = True
                lbltitle.InnerText = String.Format("Ticket '{0}' Edit (Created on: {1})", ticketid, rec.CreateDate)

            Else

                lbltitle.InnerText = "Create a new Support Ticket"
                ddlstatus.SelectedValue = "NEW"
                ddlstatus.Enabled = False



            End If



        End If

    End Sub



    Protected Sub btnsave_Click(sender As Object, e As EventArgs)
        Dim A As New cls_tickets
        Dim ticketid As String = Request.QueryString("edit-id")
        If ticketid <> "" Then
            'update ticket records

            Dim record As GHD5.Ticket = A.SelectThis(ticketid)
            Dim msgdetails As String = ""
            If record.Summary <> Trim(Me.txtsummary.Text) Then
                msgdetails = "Changed Summary from" & " " & record.Summary & " " & " to" & " " & Me.txtsummary.Text
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)
            End If
            If record.Details <> Trim(Me.txtdescription.Text) Then
                msgdetails = "Changed Description from" & " " & record.Details & " " & " to" & " " & Me.txtdescription.Text
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)
            End If
            If record.Category <> Trim(Me.ddlcategory.SelectedItem.Text) Then
                msgdetails = "Changed Category from" & " " & record.Category & " " & " to" & " " & Me.ddlcategory.SelectedItem.Text
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If
            If record.DueDate <> Trim(Me.duedate.Text) Then
                msgdetails = "Changed Due date from" & " " & record.DueDate & " " & " to" & " " & Me.duedate.Text
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If
            If record.CallType <> Trim(Me.ddlcalltype.SelectedItem.Text) Then
                msgdetails = "Changed Call type from" & " " & record.CallType & " " & " to" & " " & Me.ddlcalltype.SelectedItem.Text
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If

            If record.Assignee <> Trim(Me.ddlassignee.Value) Then
                msgdetails = "Reassigned Ticket from " & " " & record.Assignee & " " & " to" & " " & Me.ddlassignee.Value
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If

            If record.Status <> Trim(Me.ddlstatus.SelectedValue) Then
                msgdetails = "Changed status from " & " " & record.Status & " " & " to" & " " & Me.ddlstatus.SelectedItem.Text
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If

            If record.Status = "RESOLVED" And Trim(Me.ddlstatus.SelectedValue = "INPROG") Then
                msgdetails = "Reopened the ticket"
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If

            If record.Priority <> Trim(Me.ddlpriority.SelectedItem.Text) Then
                msgdetails = "Changed Priority from" & " " & record.Priority & " " & " to" & " " & Me.ddlpriority.SelectedItem.Text
                Call mod_main.InsertActivity("TICKET EDIT", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketid)

            End If
            record.Summary = txtsummary.Text.ToUpper
            record.Details = txtdescription.Text
            record.Assignee = ddlassignee.Value
            record.DueDate = duedate.Date
            'rec.CreateDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            record.Creator = combo1.SelectedText
            Dim CM As GHD5.User = (New cls_users).SelectThisusername(record.Creator)
            record.CreatorEmail = CM.Email
            record.Category = ddlcategory.SelectedItem.Text
            record.CategoryID = ddlcategory.SelectedItem.Value
            record.Priority = ddlpriority.SelectedValue
            record.CallType = ddlcalltype.SelectedValue
            record.Field1 = txtccemail.Text

            record.SubmittedBy = Session("uname")
            record.UpdatedDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            'rec.SubmittedOn = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")

            'If the ticket is in queued and user wants to start workin on it then update actual start else just change status
            If record.Status = "QUEUED" And ddlstatus.SelectedValue = "INPROG" Then
                record.ActualStart = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
                record.Status = "INPROG"
            Else
                record.Status = ddlstatus.SelectedValue
            End If

            If ddlstatus.SelectedValue = "RESOLVED" Then
                record.Active = 0
                'record.CloseDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
                record.ActualFinish = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
                record.Dueclosedate = DateTime.Now.AddDays(10)

            End If
            If ddlstatus.SelectedValue = "INPROG" Then
                record.Active = 1
            End If
            Dim res2 As ResponseInfo = A.Update(record)
            If res2.ErrorCode = 0 Then
                'save screenshot if user click upload
                If ASPxUploadControl1.PostedFile.FileName <> "" Then

                    For Each file In ASPxUploadControl1.UploadedFiles
                        file.SaveAs(Request.PhysicalApplicationPath + "/Attach/" + file.FileName)
                        Dim bb As String = "/Attach/" + file.FileName
                        Call mod_main.InsertActivity("" & Session("uname") & " Added a Photo", "SERVICEDESK", "TICKET", "", Session("uname"), record.TicketID, bb.ToString, file.FileName)

                    Next

                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Ticket Updated successfully');window.location='opentickets.aspx';", True)


                End If
            Else
                Msgbox1.ShowError(res2.ErrorMessage & " " & res2.ExtraMessage)

            End If

        Else
            'Create fresh ticket and automatically set status to 'INPROGRESS' and also start ACTUAL TIME
            Dim _Connection As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)

            _Connection.Open()
            Dim SqlNo As String = "select Nextvalue from Autosettings where NumberType = 'TicketID' "
            Dim adap As SqlDataAdapter
            Dim ds As New DataSet
            adap = New SqlDataAdapter(SqlNo, _Connection)
            adap.Fill(ds, "describe")
            Dim NValue As Integer = (ds.Tables("describe").Rows(0).Item(0) & vbNullString)

            _Connection.Close()

            'Get the abbreviation of the selected ticket category so as to use it for the autonumber too
            Dim abbr As GHD5.TicketCategory = (New cls_ticketcategories).SelectThisID(ddlcategory.SelectedValue)
            Dim cat As String = abbr.Remarks

            Dim ID As String = "" & cat & "/" & Date.Now.Month & "/" & Date.Now.Year & "/" & Format(NValue, "00")
            'Add 1 to the next number value
            _Connection.Open()
            Dim sqlNoUpD As String = "Update Autosettings set Nextvalue=Nextvalue+1 where NumberType='TicketID' "
            Dim comm1 As New SqlCommand(sqlNoUpD, _Connection)
            comm1.CommandType = CommandType.Text
            comm1.ExecuteNonQuery()
            _Connection.Close()



            Dim rec As New GHD5.Ticket

            rec.TicketID = ID
            rec.Summary = txtsummary.Text.ToUpper
            rec.Details = txtdescription.Text
            rec.Assignee = ddlassignee.Value
            rec.DueDate = duedate.Date
            rec.CreateDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            rec.Creator = combo1.SelectedText
            rec.CategoryID = ddlcategory.SelectedValue
            rec.Category = ddlcategory.SelectedItem.Text
            rec.Priority = ddlpriority.SelectedValue
            rec.Field1 = txtccemail.Text
            rec.SubmittedBy = Session("uname")
            rec.SubmittedOn = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            rec.Status = "INPROG"
            rec.ActualStart = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            rec.Active = 1
            rec.UpdatedDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            rec.TransGUID = System.Guid.NewGuid.ToString
            Dim assemail As GHD5.User = (New cls_users).SelectThisusername(rec.Assignee)
            rec.AssigneeEmail = assemail.Email
            Dim creatoremail As GHD5.User = (New cls_users).SelectThisusername(rec.Creator)

            Dim res As ResponseInfo = A.Insert(rec)
            If res.ErrorCode = 0 Then

                'save attachment
                If ASPxUploadControl1.PostedFile.FileName <> "" Then

                    For Each file In ASPxUploadControl1.UploadedFiles
                        file.SaveAs(Request.PhysicalApplicationPath + "/Attach/" + file.FileName)
                        Dim bb As String = "/Attach/" + file.FileName
                        Call mod_main.InsertActivity("" & Session("uname") & " Added a Photo", "SERVICEDESK", "TICKET", "", Session("uname"), rec.TicketID, bb.ToString, file.FileName)

                    Next

                End If


                'save activity

                Dim msgdetails As String = ""
                msgdetails = String.Format("Created the ticket: '{0}'", rec.Summary)
                Call mod_main.InsertActivity("TICKET CREATE", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), rec.TicketID)
                'populate html body email
                Dim url As String = "" & ConfigurationManager.AppSettings("pathtouserticket") & "?edit-id=" & ID & ""
                Dim body As String = mod_main.PopulateBody(ID, rec.Creator, rec.Summary, rec.Details, rec.Priority, rec.Assignee, url)
                'get the email address of the Assignee


                If mod_main.SendHtmlFormattedEmail(creatoremail.Email, rec.Field1, "Your Ticket has been Created!", body) = True Then
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully');window.location='Tickets.aspx';", True)
                Else
                    ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Data saved successfully but Mail not sent');window.location='Tickets.aspx';", True)


                End If


            Else
                Msgbox1.ShowError(res.ErrorMessage & " " & res.ExtraMessage)

            End If

        End If







        'get uploaded file and save it to path

        'Get the least admin with lowest number of open tickets
        'Dim assid As String = ""
        '_Connection.Open()
        'Dim comm2 As New SqlCommand("GetAssigneeCount", _Connection)
        'comm2.CommandType = CommandType.StoredProcedure
        'comm2.Parameters.AddWithValue("@CategoryID", ddlcategory.SelectedValue)
        'comm2.ExecuteNonQuery()
        'Dim dr As SqlDataReader
        'dr = comm2.ExecuteReader()
        'If dr.Read Then
        '    assid = dr("ASSIGNEE")

        'Else

        '    'if theres no record get assignee from user categories table randomly
        '    Dim _Connection2 As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)
        '    _Connection2.Open()
        '    Dim comm3 As New SqlCommand("SELECT username FROM UserCategories where CategoryID=@CategoryID ORDER BY NEWID()", _Connection2)
        '    comm3.CommandType = CommandType.Text
        '    comm3.Parameters.AddWithValue("@CategoryID", ddlcategory.SelectedValue)
        '    comm3.ExecuteNonQuery()
        '    Dim dr2 As SqlDataReader
        '    dr2 = comm3.ExecuteReader()
        '    If dr2.Read Then
        '        assid = dr2("Username")
        '    Else
        '        Msgbox1.ShowHelp("There's no Agent found in this Category. Contact your Administrator")
        '        Exit Sub

        '    End If

        'End If
    End Sub

    Protected Sub btncancel_Click(sender As Object, e As EventArgs)
        Response.Redirect("/Webroot/dashboard.aspx")
    End Sub



    'Protected Sub ddlcategory_SelectedIndexChanged(sender As Object, e As EventArgs)
    '    Dim TK As GHD5.TicketCategory = (New cls_ticketcategories).SelectThisID(ddlcategory.SelectedValue)
    '    mod_filldropdowns.FillAssignees(ddlassignee, TK.CategoryID)
    '    If ddlassignee.Items.Count = 0 Then
    '        Msgbox1.ShowHelp("No Agent for " & ddlcategory.SelectedValue & "")
    '        'ElseIf ddlassignee.Items.Count > 1 Then
    '        '    For Each rec As GHD5.User In ddlassignee.Items
    '        '        Dim rc As List(Of GHD5.Ticket) = (New cls_tickets).SelectAssigneetickets(rec.Username)
    '        '        If rc.Count > 0 Then
    '        '            Dim count As Integer = rc.Count
    '        '        End If
    '        '    Next
    '        Exit Sub

    '    End If
    'End Sub




    Protected Sub btnreply_Click(sender As Object, e As EventArgs)
        Dim ticketid As String = Request.QueryString("edit-id")
        Dim U As New cls_tickets
        Dim tk As GHD5.Ticket = U.SelectThis(ticketid)
        Dim B As New GHD5.Ticket
        tk.UpdatedDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
        Try
            U.Update(tk)

            Call mod_main.InsertActivity("" & Session("uname") & " Added a comment", "SERVICEDESK", "TICKET", txtreply.Text, Session("uname"), tk.TicketID)
            Dim url As String = "" & ConfigurationManager.AppSettings("pathtouserticket") & "?edit-id=" & tk.TicketID & ""
            Dim body As String = mod_main.PopulateBodyreplyadmin(ticketid, tk.Creator, tk.Summary, txtreply.Text, tk.Priority, tk.Assignee, url, tk.UpdatedDate)
            Dim use As GHD5.User = (New cls_users).SelectThisusername(tk.Creator)
            If mod_main.SendHtmlFormattedEmail(use.Email, tk.Field1, "" & tk.Assignee & " Replied your Ticket", body) = True Then
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Comment added successfully');window.location='Tickets.aspx';", True)
            Else
                ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Comment added but Mail not sent');window.location='Tickets.aspx';", True)


            End If
        Catch ex As Exception
            Msgbox1.ShowError(ex.Message)
        End Try







    End Sub



    Private Sub bindcomments(ByVal ticketid As String)

        Dim dt As New DataTable()
        Dim cmd As SqlCommand = Nothing
        Dim adp As SqlDataAdapter = Nothing
        Try

            cmd = New SqlCommand("SELECT * FROM Activity where Userid=@id order by AuditLogID DESC", _Connection)
            cmd.Parameters.AddWithValue("@id", ticketid)
            cmd.CommandType = CommandType.Text
            adp = New SqlDataAdapter(cmd)
            adp.Fill(dt)

            If dt.Rows.Count > 0 Then
                Repeater1.DataSource = dt
                Repeater1.DataBind()
            Else
                Label1.Visible = True
                Label1.Text = "No recent activities"

            End If
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "Message", "alert('Oops!! Error occured: " + ex.Message.ToString() + "');", True)
        Finally
            _Connection.Close()
            cmd.Dispose()
            adp = Nothing
            dt.Clear()
            dt.Dispose()
        End Try

    End Sub



    Private Sub bindtime(ByVal time As Date)
        Dim t As TimeSpan = DateTime.Now - time
        If t.TotalSeconds < 60 Then
            lblactivity.InnerHtml = "Less than a minute ago"
        ElseIf t.TotalMinutes < 60 Then
            lblactivity.InnerHtml = t.Minutes.ToString() + " minutes ago"
        ElseIf t.TotalHours < 24 Then
            lblactivity.InnerHtml = t.Hours.ToString + " Hours ago"
        ElseIf t.TotalDays < 7 Then
            lblactivity.InnerHtml = t.Days.ToString + " Days ago"
        ElseIf t.TotalDays > 7 Then
            lblactivity.InnerHtml = t.Days.ToString + " Weeks ago"
        ElseIf t.TotalDays > 31 Then
            lblactivity.InnerHtml = t.Days.ToString + " Months ago"
        ElseIf t.TotalDays > 365 Then
            lblactivity.InnerHtml = t.Days.ToString + " Years ago"




        End If
    End Sub

    Protected Sub ASPxUploadControl1_FileUploadComplete(sender As Object, e As FileUploadCompleteEventArgs)

    End Sub
End Class
