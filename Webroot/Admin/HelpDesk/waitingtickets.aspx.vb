﻿Imports System.IO
Imports System.Drawing
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Partial Class Webroot_Admin_HelpDesk_waitingtickets
    Inherits System.Web.UI.Page
    Public Shared _Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("GuruHelpDeskConnectionString").ConnectionString)
    Protected Sub btnnew_Click(sender As Object, e As EventArgs)
        Response.Redirect("/webroot/Admin/helpdesk/Ticket_new.aspx")
    End Sub



    Protected Sub btndelete_Click(sender As Object, e As EventArgs)
        Dim count As Integer = 0
        For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
            count += 1
            Dim ticketno As String = ASPxGridView1.GetSelectedFieldValues("TicketID")(i).ToString
            Dim msgdetails As String = ""
            msgdetails = "Deleted Ticket" & ticketno & ""


            _Connection.Open()
            Dim _cmd As SqlCommand = _Connection.CreateCommand()
            _cmd.CommandType = CommandType.Text
            _cmd.CommandText = "delete from Tickets where TicketID=('" & ticketno & "')"
            _cmd.ExecuteNonQuery()
            _Connection.Close()
            Call mod_main.InsertActivity("DELETE", "SERVICEDESK", "TICKET", msgdetails, Session("uname"))
            'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)

            Msgbox1.Showsuccess("" & count & " Ticket(s) deleted successfully!")



        Next

    End Sub

    Protected Sub reassign_Click(sender As Object, e As EventArgs)
        Dim ticketno As String = ASPxGridView1.GetSelectedFieldValues("TicketID").ToString
        Response.Redirect("/webroot/Admin/helpdesk/Ticket_new.aspx?edit-id=" & ticketno & "")
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.Msgbox1.ShowError("Please select an item")
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim ticketno As String = ASPxGridView1.GetSelectedFieldValues("TicketID")(i).ToString
                Dim rec As GHD5.Ticket = (New cls_tickets).SelectThis(ticketno)
                Dim A As New cls_tickets
                Dim msgdetails As String = ""
                msgdetails = "Changed Priority from" & " " & rec.Priority & " " & " to" & " " & Me.ddlpriority.SelectedItem.Text
                rec.Priority = ddlpriority.SelectedItem.Text
                Dim res As ResponseInfo = A.Update(rec)
                If res.ErrorCode = 0 Then
                    Call mod_main.InsertActivity("PRIORITY", "SERVICEDESK", "TICKET", msgdetails, Session("uname"))
                    Msgbox1.Showsuccess("" & count & " Ticket(s) priority set as " & rec.Priority & "")
                End If

                'ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)





            Next

        End If

    End Sub

    Protected Sub Close_Click(sender As Object, e As EventArgs)
        Dim count As Integer = ASPxGridView1.Selection.Count
        If count = 0 Then
            Me.Msgbox1.ShowError("Please select an item to Change status")
        Else
            For i As Integer = 0 To ASPxGridView1.Selection.Count - 1
                count += 1
                Dim ticketno As String = ASPxGridView1.GetSelectedFieldValues("TicketID")(i).ToString
                Dim rec As GHD5.Ticket = (New cls_tickets).SelectThis(ticketno)
                Dim msgdetails As String = ""
                msgdetails = "Changed Status from" & " " & rec.Status & " " & " to" & " CLOSED"
                Call mod_main.InsertActivity("STATUS", "SERVICEDESK", "TICKET", msgdetails, Session("uname"), ticketno)
                Try
                    Dim Conn As New SqlConnection(m_strconnString)
                    Conn.Open()
                    Dim comm2 As New SqlCommand("Update Tickets set status=@status, closedate=@closedate where TicketID=@ticketid", Conn)
                    comm2.CommandType = CommandType.Text
                    comm2.Parameters.AddWithValue("@status", "CLOSED")
                    comm2.Parameters.AddWithValue("@closedate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
                    comm2.Parameters.AddWithValue("@ticketid", ticketno)

                    comm2.ExecuteNonQuery()
                    Dim url As String = "" & ConfigurationManager.AppSettings("pathtouserclosedticket") & ""

                    Dim body As String = mod_main.PopulateBodyClosed(ticketno, rec.Creator.ToUpper, rec.Summary.ToUpper, rec.Details, rec.Priority, rec.Assignee, "http://localhost:1758/Login.aspx?goto=http://localhost:1758/webroot/helpdesk/closedtickets.aspx")
                    'get the email address of the Assignee
                    Dim use As GHD5.User = (New cls_users).SelectThisusername(rec.Creator)
                    If mod_main.SendHtmlFormattedEmail(use.Email, rec.Field1, "Ticket closed!", body) = True Then
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Ticket(s) closed successfully');window.location='ClosedTickets.aspx';", True)

                    Else
                        ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Ticket(s) closed successfully but Mail not sent');window.location='ClosedTickets.aspx';", True)

                    End If
                Catch ex As Exception
                    Msgbox1.ShowError(ex.Message)
                End Try

            Next
        End If


        'Dim A As New cls_tickets
        'Dim msgdetails As String = ""
        'msgdetails = "Changed Status from" & " " & rec.Status & " " & " to" & " CLOSED"
        'rec.Status = "CLOSED"
        'rec.CloseDate = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
        'Dim res As ResponseInfo = A.Update(rec)
        'If res.ErrorCode = 0 Then


        'Else


        'End If

        ''ClientScript.RegisterStartupScript(Page.[GetType](), "alert", "alert('Student(s) Deleted!');window.location='Students.aspx';", True)



    End Sub
End Class
