﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="Ticket_new.aspx.vb" Inherits="Webroot_Admin_HelpDesk_Ticket_new" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/users.ascx" TagName="user" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <%-- <link rel="stylesheet" type="text/css" href="/css/default.css" />--%>
    <link rel="stylesheet" type="text/css" href="/css/component.css" />
    <script src="js/modernizr.custom.js"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
        </ContentTemplate>

    </asp:UpdatePanel>


    <h3 id="lbltitle" runat="server" class="title fg-blue margin20"></h3>
    <div data-role="panel" class="panel margin20">


        <div style="z-index: 999;" class="content">
           
                   <h5 class="hint-title">Ticket Details</h5>
            <div style="margin-top:-28px;" class="place-right bg-red fg-white">
                <span class="text-small text-bold">Last Activity: </span><span runat="server" id="lblactivity" class="text-small"></span>
                <span id="spantimespent" runat="server" visible="false" class="text-small text-bold">Time Spent: <span id="lbltimespent" visible="false" runat="server" class="text-small"></span></span>
            </div>
          
         
            <hr />
               <div class="popover marker-on-bottom bg-blue">
                          
                            <p class="text-small align-center fg-white align-justify text-bold">You Can Copy multiple E-Mails by seperating them by comma(,).</p>
                        </div>

            <div class="margin20">
                <table width="100%">

                            <tr>
                                <td>
                                    <p class="text-small text-bold">Creator: </p>
                                </td>
                                <td>
                                    <span>
                                        <uc2:user ID="combo1" runat="server"></uc2:user>
                                    </span>

                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <p class="text-small text-bold">Cc Email: </p>
                                </td>
                                <td>
                                    <span>
                                        <asp:TextBox ID="txtccemail" Width="170px" CssClass="text-secondary shadow" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>
                                    </span>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <p class="text-bold text-small">Summary:</p>
                                </td>

                                <td>
                                    <span data-role="hint"
                                        data-hint-background="bg-green"
                                        data-hint-color="fg-white"
                                        data-hint-mode="2"
                                        data-hint="Ticket Subject">
                                        <asp:TextBox ID="txtsummary" CssClass="text-secondary shadow" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>
                                    </span>


                                </td>

                            </tr>
                            <tr>
                                <td><span class="text-bold text-small">Description:</span></td>
                                <td>


                                    <asp:TextBox ID="txtdescription" Height="70px" TextMode="MultiLine" Width="320px" CssClass="text-secondary shadow" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>

                                </td>

                            </tr>
                        </table>
            </div>


           <div class="margin20">
                <h5 class="hint-text">Other Details</h5>
            <hr />
           </div>
           


            <div class="margin20">
                <table width="100%">
                <tr>

                    <td>
                        <p class="text-small text-bold">Category: </p>
                    </td>
                    <td>
                        <span data-role="hint"
                            data-hint-background="bg-green"
                            data-hint-color="fg-white"
                            data-hint-mode="2"
                            data-hint="Category">
                            <asp:DropDownList ID="ddlcategory" CssClass="text-secondary shadow" BorderColor="Orange" Width="170px" runat="server">
                            </asp:DropDownList>
                        </span>
                    </td>

                    <td>
                        <p visible="true" class="text-small text-bold">Assignee: </p>
                    </td>
                    <td>
                        <span>
                            <dx:ASPxComboBox ID="ddlassignee" Theme="PlasticBlue" CssClass="shadow" runat="server" ValueType="System.String"></dx:ASPxComboBox>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p visible="true" class="text-small text-bold">Due Date: </p>
                    </td>
                    <td>
                        <span>
                            <dx:ASPxDateEdit ID="duedate" TimeSectionProperties-Visible="true" CssClass="text-secondary shadow" EditFormatString="dd-MMM-yyyy hh:mm tt" runat="server" EnableTheming="True" Theme="PlasticBlue" EditFormat="Custom" Width="170px">
                                <TimeSectionProperties>
                                    <TimeEditProperties EditFormatString="hh:mm tt" />
                                </TimeSectionProperties>
                            </dx:ASPxDateEdit>
                        </span>
                    </td>
                    <td>
                        <p visible="true" class="text-small text-bold">Call Type: </p>
                    </td>
                    <td>
                        <span>
                            <asp:DropDownList ID="ddlcalltype" CssClass="text-secondary shadow" BorderColor="Orange" Width="170px" runat="server"></asp:DropDownList></span>

                    </td>
                </tr>

                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <tr>
                             <td>
                        <p visible="true" class="text-small text-bold">STATUS: </p>
                    </td>
                    <td>
                        <span>
                            <asp:DropDownList ID="ddlstatus" CssClass="text-secondary shadow" BorderColor="Orange" Width="170px" runat="server">
                                <asp:ListItem Value="NEW">NEW</asp:ListItem>
                                <asp:ListItem Value="QUEUED">QUEUED</asp:ListItem>
                                <asp:ListItem Value="INPROG">IN PROGRESS</asp:ListItem>
                                 <asp:ListItem Value="PENDING">WAITING ON USER</asp:ListItem>
                                <asp:ListItem Value="RESOLVED">RESOLVED</asp:ListItem>
                            </asp:DropDownList></span>

                    </td>

                            <td>
                                <p runat="server" visible="true" class="text-small text-bold">Priority: </p>
                            </td>
                            <td><span>
                                <asp:DropDownList ID="ddlpriority" CssClass="text-secondary shadow" BorderColor="Orange" Width="170px" runat="server">
                                    <asp:ListItem Enabled="true">--Select Priority--</asp:ListItem>
                                    <asp:ListItem>High</asp:ListItem>
                                    <asp:ListItem>Medium</asp:ListItem>
                                    <asp:ListItem>Low</asp:ListItem>
                                </asp:DropDownList></span></td>

                        </tr>
                        <tr>
                             <td>
                                <p class="text-small text-bold">Upload Attachment: </p>
                            </td>
                            <td>
                               <span>
                                       <dx:ASPxUploadControl ID="ASPxUploadControl1" NullText="Upload file" CssClass="shadow" OnFileUploadComplete="ASPxUploadControl1_FileUploadComplete" runat="server" UploadMode="Standard">
                               <ValidationSettings AllowedFileExtensions=".txt,.jpg,.jpe,.jpeg,.doc,.png" MaxFileSize="1000000">
                                  
                               </ValidationSettings>
                           </dx:ASPxUploadControl>
                                  </span>
                          
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </table>
            </div>
            


            <br />
            <asp:Panel ID="Panel1" Visible="false" runat="server">



                <div class="input-control full-size text">
                    <asp:TextBox placeholder="Public Response..." ID="txtreply" CssClass="text-secondary shadow" BorderColor="red" BorderWidth="1" runat="server"></asp:TextBox>

                </div>
                <asp:Label ID="Label1" runat="server" CssClass="text-secondary notify-text fg-brown" Visible="false" Text=""></asp:Label>
                <br />
                <p>
                    <asp:Button ID="btnreply" Visible="true" Height="29px" OnClick="btnreply_Click" CssClass="button shadow text-small bg-green fg-white mini-button" runat="server" Text="Update" Font-Size="X-Small" Font-Bold="True" /></p>

                <h5 class="hint-text">Ticket Activities</h5>
                <hr />


                <ul class="cbp_tmtimeline">
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <li>
                                <time class="cbp_tmtime" datetime="<%#Eval("SubmittedOn", "{0:MMMM d, yyyy}")%>"><span><%#Eval("SubmittedOn", "{0:d/MM/yy}")%></span> <span><%#Eval("SubmittedOn", "{0:HH:mm}")%></span></time>
                                <div class="cbp_tmicon cbp_tmicon-earth"></div>
                                <div class="cbp_tmlabel">
                                    <h2 class="text-secondary"><%#Eval("LogType")%></h2>
                                    <asp:panel id="Panel1" runat="server" visible='<%# Not [String].IsNullOrEmpty(If(TypeOf Eval("Filepath") Is DBNull, [String].Empty, DirectCast(Eval("Filepath"), String)))%>'>
                                    <p><a href="<%#Eval("Filepath")%>" download="<%#Eval("Filename")%>">
                                        <img width="100" height="200" src="<%#Eval("Filepath")%>" /></a></p>
                                    </asp:panel>
                                     <asp:panel id="Panel2" runat="server" visible='<%# Not [String].IsNullOrEmpty(If(TypeOf Eval("Description") Is DBNull, [String].Empty, DirectCast(Eval("Description"), String)))%>'>
                                     <p><b><%#Eval("SubmittedBy")%></b> <%#Eval("Description")%></p>
                                    </asp:panel>
                                 
                                </div>
                            </li>
                            
                        </ItemTemplate>
                    </asp:Repeater>

                </ul>

            </asp:Panel>

        </div>


        <div class="align-center">
            <span>&nbsp;
                     <asp:Button ID="btnsave" Visible="true" Height="29px" OnClick="btnsave_Click" CssClass="button text-small bg-green fg-white mini-button" runat="server" Text="Save" Font-Size="X-Small" Font-Bold="True" />

            </span>

            <span>
                <asp:Button ID="btncancel" Visible="true" Height="29px" OnClick="btncancel_Click" CssClass="button text-small bg-darkRed fg-white mini-button" runat="server" Text="Cancel" Font-Size="X-Small" Font-Bold="True" />

            </span>

        </div>
    </div>
</asp:Content>

