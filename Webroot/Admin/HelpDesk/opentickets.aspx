﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="opentickets.aspx.vb" Inherits="Webroot_Admin_HelpDesk_opentickets" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
        </ContentTemplate>

    </asp:UpdatePanel>
    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" ConfirmText="Are you sure you want to delete this Ticket" TargetControlID="btndelete" runat="server" />


    <div>
        <h4 class="title fg-blue margin20">Tickets</h4>
        <div class="panel margin20">
            <div class="content">
                <h5 class="hint-title">My Open Tickets</h5>
                <hr />
                <div class="popover marker-on-bottom bg-red">
                    <div class="fg-white text-small text-bold">What are you looking for? Enter a keyword in the provided textbox below to search for a Ticket.</div>
                </div>
                <dx:ASPxGridView ID="ASPxGridView1" Styles-Header-Font-Bold="true" Width="100%" Styles-SelectedRow-ForeColor="White" Styles-SelectedRow-CssClass="fg-white bg-red" SettingsPager-PageSize="7" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="SN" Theme="PlasticBlue" Settings-GridLines="Horizontal" EnableTheming="True">


                    <SettingsPager PageSize="7"></SettingsPager>

                    <Settings ShowFilterBar="Auto" />
                    <SettingsSearchPanel Visible="True" />
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataHyperLinkColumn FieldName="TicketID" PropertiesHyperLinkEdit-Style-CssClass=" text-small text-bold" PropertiesHyperLinkEdit-DisplayFormatString="TicketID" PropertiesHyperLinkEdit-NavigateUrlFormatString="Ticket_new.aspx?edit-id={0}" VisibleIndex="1">
                            <PropertiesHyperLinkEdit NavigateUrlFormatString="Ticket_new.aspx?edit-id={0}" TextFormatString="TicketID">
                            </PropertiesHyperLinkEdit>


                        </dx:GridViewDataHyperLinkColumn>
                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Summary" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>

                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small" FieldName="CreateDate" VisibleIndex="2">
                        </dx:GridViewDataDateColumn>

                            <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Creator" VisibleIndex="3">
                       </dx:GridViewDataTextColumn>

                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Assignee" VisibleIndex="3">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Priority" VisibleIndex="4">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn CellStyle-CssClass="text-small" FieldName="Status" VisibleIndex="5">
                        </dx:GridViewDataTextColumn>



                        <dx:GridViewDataDateColumn PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy" CellStyle-CssClass="text-small" FieldName="DueDate" VisibleIndex="6">
                        </dx:GridViewDataDateColumn>


                    </Columns>

                    <Styles>
                        <AlternatingRow Enabled="true" />

                        <SelectedRow ForeColor="White"></SelectedRow>

                    </Styles>
                </dx:ASPxGridView>


                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:GuruHelpDeskConnectionString %>" SelectCommand="SELECT * FROM [Tickets] WHERE ([Assignee] = @Assignee and [Active] = '1') ORDER BY [CreateDate] DESC">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="Assignee" SessionField="uname" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>


                <div class="align-center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnnew" Visible="true" Height="29px" OnClick="btnnew_Click" CssClass="button text-small bg-lightGreen fg-white mini-button" runat="server" Text="New Ticket" Font-Size="X-Small" Font-Bold="True" />

                            </td>
                            <td>
                                <asp:Button ID="reassign" Visible="false" Height="29px" OnClick="reassign_Click" CssClass="button text-small bg-darkOrange fg-white mini-button" runat="server" Text="Reassign Ticket" Font-Size="X-Small" Font-Bold="True" />

                            </td>

                            <td>

                                <asp:Button ID="Close" Visible="false" Height="29px" OnClick="Close_Click" CssClass="button bg-darkBlue fg-white mini-button" runat="server" Text="Close Ticket" Font-Size="X-Small" Font-Bold="True" />
                            </td>

                            <td>
                                <asp:DropDownList ID="ddlpriority" Visible="false" AutoPostBack="true" Width="170px" BorderColor="orange" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" runat="server">
                                    <asp:ListItem Value="null" Enabled="true">---Select---</asp:ListItem>
                                    <asp:ListItem>EMERGENCY!</asp:ListItem>
                                    <asp:ListItem>URGENT</asp:ListItem>
                                    <asp:ListItem>HIGH</asp:ListItem>
                                    <asp:ListItem>NORMAL</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btncancel" Visible="true" Height="29px" OnClientClick="history.go(-1)" CssClass="button text-small bg-red fg-white mini-button" runat="server" Text="Cancel" ToolTip="Cancel" Font-Size="X-Small" Font-Bold="True" />

                            </td>
                            <td>
                                <asp:Button ID="btndelete" Visible="false" Height="29px" OnClick="btndelete_Click" CssClass="button bg-lightRed fg-white mini-button" runat="server" Text="Delete" Font-Size="X-Small" Font-Bold="True" />
                            </td>

                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

