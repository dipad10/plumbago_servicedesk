﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="Activity.aspx.vb" Inherits="Webroot_Admin_HelpDesk_Activity" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/messagebox.ascx" TagName="msgbox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
                 <uc1:msgbox ID="Msgbox1" runat="server"></uc1:msgbox>
           </ContentTemplate>
         
       </asp:UpdatePanel>
    <div class="margin20">
         <h4 class="title margin20">Latest Update for all tickets</h4>
               
       <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:Repeater ID="Repeater1" runat="server">
    <ItemTemplate>
   <a href="Ticket_new.aspx?edit-id=<%#Eval("UserID")%>"><div class="panel">  
                <div class="content hovered hover bg-hover-grayLight">
                <div class="place-right"><span class="text-small"><%#Eval("SubmittedOn", "{0:MMMM d, yyyy}")%> @ <%#Eval("SubmittedOn", "{0:hh:mm tt}")%></span>
                    <p class="text-small text-bold"><%#Eval("UserID")%></p>
                </div>
                <div style="width:40px;" class="place-left"><span class="mif-user fg-green"></span></div>
                <div style="margin:0px 40px;">
                    <span class="text-small text-bold"><%#Eval("Category")%> <%#Eval("LogType")%></span>
                    <p class="text-small"><%#Eval("SubmittedBy")%> <%#Eval("Description")%></p>
                </div>
            </div>
          <hr style="height:1px;" />
            
          </div></a>    
    </ItemTemplate>
</asp:Repeater>
  <asp:Button ID="btnloadmore" Visible="true" Height="29px" OnClick="Btnloadmore_Click" CssClass="button text-small bg-darkRed fg-white mini-button" runat="server" Text="Load More" Font-Size="X-Small" Font-Bold="True" />
                                     <asp:UpdateProgress ID="UpdateProgress1" ClientIDMode="Static" DisplayAfter="5" runat="server">
                                <ProgressTemplate>
                                    <img src="/images/728.gif" style="text-align:center;" alt="wait" width="30" height="30" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                                </ContentTemplate>
                            </asp:UpdatePanel>
      
        
        
         
    </div>

</asp:Content>

