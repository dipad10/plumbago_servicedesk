﻿Option Compare Text
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports System.IO.Ports
Imports System.Threading
Imports DevExpress.XtraReports.UI
Imports DevExpress.Web
Imports System.IO
Imports DevExpress.XtraReports.Web

Partial Class Webroot_Admin_Reports_Report
    Inherits System.Web.UI.Page
    Private Function GetReportByName(ByVal reportName As String) As XtraReport
        Dim report As XtraReport = Nothing
        Select Case reportName
            Case "RptGetAllReportsByPeriod"
                report = New RptGetAllReportsByPeriod()
            Case "AllTicketsByStatus"
                report = New AllTicketsByStatus()
            Case "AllTicketCategoryIssuedToAssigneebyStatus"
                report = New AllTicketCategoryIssuedToAssigneebyStatus()
            Case "AllTicketsbyCategory"
                report = New AllTicketsbyCategory
            Case "AllTicketsbyAssignee"
                report = New AllTicketsbyAssignee
            Case "AllTicketsGroupedbyCategories"
                report = New AllTicketsGroupedbyCategories
            Case "AllTicketsGroupedbyAssignee"
                report = New AllTicketsGroupedbyAssignee


        End Select
        Return report
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim reportName As String = CStr(Session("ReportName"))
        If (Not String.IsNullOrEmpty(reportName)) Then
            ASPxDocumentViewer1.Report = GetReportByName(reportName)
        End If


    End Sub

    Protected Sub btPreview_Click(sender As Object, e As EventArgs)
        Dim reportName As String = CStr(cbReportType.Value)

        ASPxDocumentViewer1.Report = GetReportByName(reportName)
        Session("ReportName") = reportName


    End Sub
    Sub ShowPreview()
        'Response.Redirect("ASPNETDocumentViewer.aspx")
        Response.Redirect("/webroot/Admin/Report/Viewer.aspx")
    End Sub


    'get parameter names and do dropdown
    Protected Sub ASPxDocumentViewer1_CustomizeParameterEditors(sender As Object, e As CustomizeParameterEditorsEventArgs)
        Select Case e.Parameter.Name
            Case "Assignee"
                Dim customParameterEditor As New ASPxComboBox() With {.ID = "assignee"}
                e.Editor = customParameterEditor
                AddHandler customParameterEditor.Init, AddressOf customParameterEditor_Init
            Case "creator"
                Dim customParameterEditor As New ASPxComboBox() With {.ID = "creator"}
                e.Editor = customParameterEditor
                AddHandler customParameterEditor.Init, AddressOf customParameterEditor_Init2

            Case "Categoryname"
                Dim customParameterEditor As New ASPxComboBox() With {.ID = "Categoryname"}
                e.Editor = customParameterEditor
                AddHandler customParameterEditor.Init, AddressOf customParameterEditor_Init3

            Case "Active"
                Dim customParameterEditor As New ASPxComboBox() With {.ID = "Active"}
                e.Editor = customParameterEditor
                AddHandler customParameterEditor.Init, AddressOf customParameterEditor_Init4
        End Select
       
    End Sub
    'if parameter name is @assignee then make dropdown
    Private Sub customParameterEditor_Init(ByVal sender As Object, ByVal e As EventArgs)
        Dim customParameterEditor As ASPxComboBox = TryCast(sender, ASPxComboBox)



        'customParameterEditor.ValueSeparator = "|"c
        Dim Recs As List(Of GHD5.User) = (New cls_users).SelectAlladminusrs
        customParameterEditor.DataSource = Recs
        customParameterEditor.Columns.Add("UserID").Width = 80
        customParameterEditor.Columns.Add("Username").Width = 120
        customParameterEditor.Columns.Add("Email").Width = 200
        customParameterEditor.ValueField = "Username"
        customParameterEditor.TextField = "UserID"

        customParameterEditor.TextFormatString = "{0},{1}"
        customParameterEditor.IncrementalFilteringMode = IncrementalFilteringMode.Contains




        customParameterEditor.DataBindItems()
    End Sub
    'if parameter name is @creator then make dropdown of all basic users
    Private Sub customParameterEditor_Init2(ByVal sender As Object, ByVal e As EventArgs)
        Dim customParameterEditor As ASPxComboBox = TryCast(sender, ASPxComboBox)



        'customParameterEditor.ValueSeparator = "|"c
        Dim Recs As List(Of GHD5.User) = (New cls_users).SelectAllbasicusers
        customParameterEditor.DataSource = Recs
        customParameterEditor.Columns.Add("UserID").Width = 80
        customParameterEditor.Columns.Add("Username").Width = 120
        customParameterEditor.Columns.Add("Email").Width = 200
        customParameterEditor.ValueField = "Username"
        customParameterEditor.TextField = "UserID"

        customParameterEditor.TextFormatString = "{0},{1}"
        customParameterEditor.IncrementalFilteringMode = IncrementalFilteringMode.Contains




        customParameterEditor.DataBindItems()
    End Sub


    Private Sub customParameterEditor_Init3(ByVal sender As Object, ByVal e As EventArgs)
        Dim customParameterEditor As ASPxComboBox = TryCast(sender, ASPxComboBox)
        Call mod_filldropdowns.FillTicketcategories(customParameterEditor)

    End Sub

    Private Sub customParameterEditor_Init4(ByVal sender As Object, ByVal e As EventArgs)
        Dim customParameterEditor As ASPxComboBox = TryCast(sender, ASPxComboBox)
        Call mod_filldropdowns.FillTicketstatus(customParameterEditor)

    End Sub


End Class
