﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Webroot/Admin/Adminmaster.master" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="Webroot_Admin_Reports_Report" %>
<%@ Register Assembly="DevExpress.XtraReports.v15.2.Web, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
     <h4 class="title margin20">All Reports</h4>
    <hr class="margin20" />
        <div class="popover margin20 marker-on-bottom bg-red">
                            <div class="fg-white text-small text-bold">To Spool out a Report, click on the dropdwown to select a report then click the "Preview Report" button to select parameters for your report</div>
                        </div>
     <table class="margin20">
            <tr>
                <td>
                    <dx:ASPxComboBox ID="cbReportType" Width="300px" Height="30px" Theme="PlasticBlue" runat="server" ValueType="System.String">
                        <Items>
                            <dx:ListEditItem Text="<---Select a Report---->" Value="" />
                            <dx:ListEditItem Text="All Tickets Reports" Value="RptGetAllReportsByPeriod" />
                            <dx:ListEditItem Text="All Tickets By Status" Value="AllTicketsByStatus" />
                           <dx:ListEditItem Text="All Tickets By Category" Value="AllTicketsbyCategory" />
                            <dx:ListEditItem Text="All Tickets By Assignee" Value="AllTicketsbyAssignee" />
                             <dx:ListEditItem Text="All Tickets Grouped By Categories" Value="AllTicketsGroupedbyCategories" />
                             <dx:ListEditItem Text="All Tickets Grouped By Assignee" Value="AllTicketsGroupedbyAssignee" />

                        </Items>
                    </dx:ASPxComboBox>
                </td>
                <td>
                   <asp:Button ID="btPreview" Visible="true" Height="29px" OnClick="btPreview_Click" CssClass="button text-small bg-darkBlue fg-white mini-button" runat="server" Text="Preview Report" Font-Size="X-Small" Font-Bold="True" />

                  
                </td>
            </tr>
        </table>
    <div class="margin10">
        <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" OnCustomizeParameterEditors="ASPxDocumentViewer1_CustomizeParameterEditors" Theme="PlasticBlue" runat="server"></dx:ASPxDocumentViewer>

    </div>
</asp:Content>

