﻿Imports Microsoft.VisualBasic
Imports System

Public Class cls_settings
    Dim DB As GHD5.DataClassesDataContext
    Public Sub New(Optional ByVal Context As GHD5.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New GHD5.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As GHD5.Setting) As ResponseInfo
        Try
            DB.Settings.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function



    Public Function Update(ByVal R As GHD5.Setting) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal sn As String) As GHD5.Setting
        Try
            Return (From R In DB.Settings Where R.SN = sn).ToList()(0)
        Catch ex As Exception
            Return New GHD5.Setting

        End Try
    End Function



    Public Function SelectAllsettings() As List(Of GHD5.Setting)
        Try
            Return (From U In DB.Settings).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.Setting)
        End Try
    End Function
End Class
