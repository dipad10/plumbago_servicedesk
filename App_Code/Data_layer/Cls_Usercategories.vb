﻿Imports Microsoft.VisualBasic

Public Class Cls_Usercategories
    Dim DB As New GHD5.DataClassesDataContext
    Public Sub New(Optional ByVal Context As GHD5.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New GHD5.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As GHD5.UserCategory) As ResponseInfo
        Try
            DB.UserCategories.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As GHD5.UserCategory) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function Selectcatid(ByVal catid As String) As List(Of GHD5.UserCategory)
        Try
            Return (From R In DB.UserCategories Where R.CategoryID = catid).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.UserCategory)

        End Try
    End Function

    Public Function SelectThisuserid(ByVal userid As String) As List(Of GHD5.UserCategory)
        Try
            Return (From R In DB.UserCategories Where R.UserID = userid).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.UserCategory)

        End Try
    End Function

    
    Public Function SelectAllusercategories() As List(Of GHD5.UserCategory)
        Try
            Return (From U In DB.UserCategories).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.UserCategory)
        End Try
    End Function

    Public Function RemoveAllusercategories(ByVal UserID As String) As ResponseInfo
        Try
            Dim rows As List(Of GHD5.UserCategory) = (From item In DB.UserCategories Where (item.UserID = UserID)).ToList()
            DB.UserCategories.DeleteAllOnSubmit(rows)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function Insertusercategories(ByVal UserID As String, ByVal CategoryID As String, ByVal categoryname As String, ByVal username As String, ByVal submittedby As String) As ResponseInfo
        Try
            Dim O As New GHD5.UserCategory
            O.UserID = UserID
            O.CategoryID = CategoryID
            O.submittedby = submittedby
            O.Username = username
            O.submittedon = Date.Now
            O.Categoryname = categoryname

            DB.UserCategories.InsertOnSubmit(O)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    'Public Function SelectByUserID(ByVal UserID As String) As List(Of GHD5.UserCategory)
    '    Try
    '        Return (From uR In DB.UserCategories, R In DB.TicketCategories _
    '                Where uR.CategoryID = R.CategoryID _
    '                AndAlso uR.UserID = UserID _
    '                Select R).ToList()
    '    Catch ex As Exception
    '        Return New List(Of GHD5.UserCategory)
    '    End Try
    'End Function
End Class
