﻿Imports Microsoft.VisualBasic
Imports System

Public Class Cls_calltypes
    Dim DB As GHD5.DataClassesDataContext
    Public Sub New(Optional ByVal Context As GHD5.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New GHD5.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As GHD5.CallType) As ResponseInfo
        Try
            DB.CallTypes.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As GHD5.CallType) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal calltypeid As String) As GHD5.CallType
        Try
            Return (From R In DB.CallTypes Where R.CallTypeID = calltypeid).ToList()(0)
        Catch ex As Exception
            Return New GHD5.CallType

        End Try
    End Function

    Public Function SelectAllcalltypes() As List(Of GHD5.CallType)
        Try
            Return (From U In DB.CallTypes).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.CallType)
        End Try
    End Function
End Class
