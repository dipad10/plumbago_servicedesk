﻿Imports Microsoft.VisualBasic

Public Class cls_tickets
    Dim db As New GHD5.DataClassesDataContext

    Public Function Delete(ByVal TicketID As String) As ResponseInfo
        Try
            Dim row As GHD5.Ticket = (From P In db.Tickets Where P.TicketID = TicketID).ToList()(0)

            db.Tickets.DeleteOnSubmit(row)
            db.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function Insert(ByVal G As GHD5.Ticket) As ResponseInfo
        Try
            db.Tickets.InsertOnSubmit(G)
            db.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function Update(ByVal G As GHD5.Ticket) As ResponseInfo
        Try
            db.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try
    End Function

    Public Function SelectThis(ByVal ticketID As String) As GHD5.Ticket
        Try
            Return (From P In db.Tickets Where P.TicketID = ticketID).ToList()(0)
        Catch ex As Exception
            Return New GHD5.Ticket
        End Try
    End Function

    Public Function SelectThisName(ByVal Details As String) As GHD5.Ticket
        Try
            Return (From I In db.Tickets Where I.Details = Details).ToList()(0)
        Catch ex As Exception
            Return New GHD5.Ticket
        End Try
    End Function

    Public Function SelectAll() As List(Of GHD5.Ticket)
        Try
            Return (From P In db.Tickets).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.Ticket)
        End Try
    End Function
    Public Function SelectAllTickets(ByVal UserID As String) As List(Of GHD5.Ticket)
        Try
            Return (From P In db.Tickets Where P.SubmittedBy = UserID).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.Ticket)
        End Try
    End Function

    Public Function SelectAssigneetickets(ByVal assignee As String) As List(Of GHD5.Ticket)
        Try
            Return (From P In db.Tickets Where P.Assignee = assignee).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.Ticket)
        End Try
    End Function

    
End Class
