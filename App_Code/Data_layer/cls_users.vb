﻿Imports Microsoft.VisualBasic

Public Class cls_users
    Dim DB As New GHD5.DataClassesDataContext
    Public Sub New(Optional ByVal Context As GHD5.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New GHD5.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As GHD5.User) As ResponseInfo
        Try
            DB.Users.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As GHD5.User) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal userid As String) As GHD5.User
        Try
            Return (From R In DB.Users Where R.UserID = userid).ToList()(0)
        Catch ex As Exception
            Return New GHD5.User

        End Try
    End Function

    Public Function SelectThisusername(ByVal username As String) As GHD5.User
        Try
            Return (From R In DB.Users Where R.Username = username).ToList()(0)
        Catch ex As Exception
            Return New GHD5.User

        End Try
    End Function

    Public Function SelectThisemail(ByVal emailaddress As String) As GHD5.User
        Try
            Return (From R In DB.Users Where R.Email = emailaddress).ToList()(0)
        Catch ex As Exception
            Return New GHD5.User

        End Try
    End Function

    Public Function SelectuserbycategoryID(ByVal categoryid As String) As List(Of GHD5.User)
        Try
            Return (From R In DB.Users Where R.Assignee = categoryid).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.User)

        End Try
    End Function
    Public Function SelectThisByRefuseridlist(ByVal userid As String) As List(Of GHD5.User)
        Try
            Return (From A In DB.Users Where A.UserID = userid).ToList()

        Catch ex As Exception
            Return New List(Of GHD5.User)
        End Try
    End Function

    Public Function SelectAllusers() As List(Of GHD5.User)
        Try
            Return (From U In DB.Users).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.User)
        End Try
    End Function

    Public Function SelectAlladminusrs() As List(Of GHD5.User)
        Try
            Return (From U In DB.Users Where U.permission = "Admin").ToList()
        Catch ex As Exception
            Return New List(Of GHD5.User)
        End Try
    End Function

    Public Function SelectAllbasicusers() As List(Of GHD5.User)
        Try
            Return (From U In DB.Users Where U.permission = "BasicUser").ToList()
        Catch ex As Exception
            Return New List(Of GHD5.User)
        End Try
    End Function
    Public Function SelectAllFilter(ByVal Filter As String) As Object
        Try
            Return (From U In DB.Users Where (U.FirstName.Contains(Filter)) Or (U.LastName.Contains(Filter)) Select U).Take(10).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.User)
        End Try
    End Function

End Class
