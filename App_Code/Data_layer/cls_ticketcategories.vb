﻿Imports Microsoft.VisualBasic
Imports System

Public Class cls_ticketcategories
    Dim DB As New GHD5.DataClassesDataContext
    Public Sub New(Optional ByVal Context As GHD5.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New GHD5.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As GHD5.TicketCategory) As ResponseInfo
        Try
            DB.TicketCategories.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As GHD5.TicketCategory) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal caategoryid As String) As GHD5.TicketCategory
        Try
            Return (From R In DB.TicketCategories Where R.CategoryID = caategoryid).ToList()(0)
        Catch ex As Exception
            Return New GHD5.TicketCategory

        End Try
    End Function
    Public Function SelectThiscategoryname(ByVal categoryname As String) As GHD5.TicketCategory
        Try
            Return (From R In DB.TicketCategories Where R.Categoryname = categoryname).ToList()(0)
        Catch ex As Exception
            Return New GHD5.TicketCategory

        End Try
    End Function
    Public Function SelectAllcategories() As List(Of GHD5.TicketCategory)
        Try
            Return (From U In DB.TicketCategories).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.TicketCategory)
        End Try
    End Function
End Class
