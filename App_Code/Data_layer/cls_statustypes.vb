﻿Imports Microsoft.VisualBasic

Public Class cls_statustypes
    Dim DB As GHD5.DataClassesDataContext
    Public Sub New(Optional ByVal Context As GHD5.DataClassesDataContext = Nothing)
        If Context Is Nothing Then
            DB = New GHD5.DataClassesDataContext
        Else
            DB = Context
        End If

    End Sub
    Public Function Insert(ByVal G As GHD5.StatusType) As ResponseInfo
        Try
            DB.StatusTypes.InsertOnSubmit(G)
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function
    Public Function Update(ByVal R As GHD5.StatusType) As ResponseInfo
        Try
            DB.SubmitChanges()
            Return mod_main._GetResponseStruct(ErrCodeEnum.NO_ERROR, 1, 0)
        Catch ex As Exception
            Return mod_main._GetResponseStruct(ErrCodeEnum.GENERIC_ERROR, , , ex.Message)
        End Try

    End Function

    Public Function SelectThisID(ByVal tag As String) As GHD5.StatusType
        Try
            Return (From R In DB.StatusTypes Where R.Tag = tag).ToList()(0)
        Catch ex As Exception
            Return New GHD5.StatusType

        End Try
    End Function

    Public Function SelectAllstatustypes() As List(Of GHD5.StatusType)
        Try
            Return (From U In DB.StatusTypes).ToList()
        Catch ex As Exception
            Return New List(Of GHD5.StatusType)
        End Try
    End Function
End Class
