﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewsWidget.ascx.vb" Inherits="Docking_Widgets_NewsWidget" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<dx:ASPxNewsControl ID="ASPxNewsControl" runat="server" NavigateUrlFormatString="javascript:void('{0}');"
    Width="100%">
    <Border BorderStyle="None"/>
    <ItemSettings ImagePosition="Left" MaxLength="120" TailText="Details">
    </ItemSettings>
    <Items>
        <dx:NewsItem Date="4/12/2011" Image-Url="../Images/Widgets/News/gagarin.jpg"
            HeaderText="50th Anniversary of Yuri Gagarin's Flight into Space"
            Text="On April 12th of 1961, Yuri Gagarin was the first human to fly into
            outer space. Today is the 50th Anniversary of his journey.">
        </dx:NewsItem>
        <dx:NewsItem Date="3/29/2011" Image-Url="../Images/Widgets/News/indices.png"
            HeaderText="U.S. Stocks Advance Tuesday"
            Text="The U.S. stock market indices made strong gains by closing time on
            Tuesday. The European stock market indices were mixed at closing.">
        </dx:NewsItem>
    </Items>
</dx:ASPxNewsControl>
