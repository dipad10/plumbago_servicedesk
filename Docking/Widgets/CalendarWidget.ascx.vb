﻿
Partial Class Docking_Widgets_CalendarWidget
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Calendar.SelectedDate = New DateTime(DateTime.Now.Year, Date.Now.Month, Date.Now.Day)
    End Sub
End Class
