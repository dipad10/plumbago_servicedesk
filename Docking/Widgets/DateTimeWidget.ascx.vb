﻿
Partial Class Docking_Widgets_DateTimeWidget
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        DateLabel.Text = New DateTime(DateTime.Now.Year, Date.Now.Month, Date.Now.Day).ToLongDateString()
        TimeLabel.Text = DateTime.Now.Hour & ":" & DateTime.Now.Minute & ":" & DateTime.Now.Second
    End Sub
End Class
