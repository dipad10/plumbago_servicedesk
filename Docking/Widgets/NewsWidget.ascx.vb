﻿Imports DevExpress.Web

Partial Class Docking_Widgets_NewsWidget
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        ASPxNewsControl.ItemSettings.DateVerticalPosition = DateVerticalPosition.BelowHeader
        ASPxNewsControl.ItemSettings.DateHorizontalPosition = DateHorizontalPosition.Right
        ASPxNewsControl.Paddings.Padding = 10
        ASPxNewsControl.ContentStyle.Paddings.Padding = 0
    End Sub
End Class
