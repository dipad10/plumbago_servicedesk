﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Navbar.ascx.vb" Inherits="Controls_Navbar" %>
<header class="app-bar fixed-top navy" data-role="appbar">
    <%--<div class="container">--%>
    <a href="/" class="app-bar-element  text-bold text-shadow branding">
        <img src="/images/servicedesk.png" style="height: 40px; display: inline-block; margin-right: 10px;">Plumbago ServiceDesk Pro</a>


    <ul class="app-bar-menu small-dropdown">
        <li runat="server" id="Home" data-flexorderorigin="12" data-flexorder="13">
            <a id="userdash" runat="server" href="/Webroot/Dashboard.aspx" class=""><span class="text-secondary text-bold">Home</span></a>
            <a id="admindash" runat="server" href="/Webroot/Admin/Home.aspx" class=""><span class="text-secondary text-bold">Dashboard</span></a>

        </li>
        <li id="modulehelpdesk" runat="server" data-flexorderorigin="1" data-flexorder="2" class="">
            <a href="#" class="dropdown-toggle text-secondary"><span class="mif-help icon text-secondary text-bold">Help Desk</span></a>
            <ul class="d-menu" data-role="dropdown" data-no-close="true" style="display: none;">

                <li class="text-secondary  text-bold"><a class=" fg-white" href="/webroot/HelpDesk/Ticket_new.aspx">New Ticket</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/webroot/HelpDesk/Tickets.aspx">My Tickets</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/webroot/HelpDesk/closedtickets.aspx">Closed Tickets</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/webroot/HelpDesk/opentickets.aspx">Open Tickets</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/webroot/HelpDesk/PastDueTickets.aspx">Past Due Tickets</a></li>

            </ul>
        </li>

        <li id="modulehelpdeskadmin" runat="server" data-flexorderorigin="1" data-flexorder="2" class="">
            <a href="#" class="dropdown-toggle text-secondary"><span class="mif-help icon text-secondary text-bold">Help Desk</span></a>
            <ul class="d-menu" data-role="dropdown" data-no-close="true" style="display: none;">

                <li class="text-secondary  text-bold"><a class=" fg-white" href="/webroot/Admin/HelpDesk/Ticket_new.aspx">New Ticket</a></li>
                <li class="text-secondary   text-bold"><a class="fg-white" href="/webroot/Admin/HelpDesk/opentickets.aspx">Open Tickets</a></li>
                 <li class="text-secondary  fg-hover-white text-bold"><a class="fg-white" href="/webroot/Admin/HelpDesk/waitingtickets.aspx">Waiting Tickets</a></li>
                <li class="text-secondary  fg-hover-white text-bold"><a class="fg-white" href="/webroot/Admin/HelpDesk/closedtickets.aspx">Closed Tickets</a></li>
                <li class="text-secondary  text-bold"><a class="fg-white" href="/webroot/Admin/HelpDesk/PastDueTickets.aspx">Past Due Tickets</a></li>
                <li class="text-secondary  fg-hover-white text-bold"><a class="fg-white" href="/webroot/Admin/HelpDesk/Tickets.aspx">All Tickets</a></li>

            </ul>
        </li>
        <li id="moduleactivity" runat="server" data-flexorderorigin="10" data-flexorder="11">
            <a href="/Webroot/Admin/HelpDesk/Activity.aspx" class=""><span class="icon mif-earth text-secondary text-bold">Activity</span></a>

        </li>

        <li id="modulereports" runat="server" data-flexorderorigin="10" data-flexorder="11">
            <a href="/webroot/Admin/Reports/Report.aspx" class=""><span class="icon mif-database text-secondary text-bold">Reports</span></a>

        </li>

        <li id="moduleadmin" runat="server" data-flexorderorigin="1" data-flexorder="2" class="">
            <a href="#" class="dropdown-toggle  text-secondary"><span class="mif-tools icon text-secondary text-bold">[Settings]</span></a>
            <ul class="d-menu" data-role="dropdown" data-no-close="true" style="display: none;">

                <li class="text-secondary text-bold"><a class=" fg-white" href="/webroot/Admin/Settings/Categories.aspx">Setup Categories</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/webroot/Admin/Settings/Users.aspx">Setup Users</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/webroot/Admin/Settings/Allocations.aspx">Set Assignee Categories</a></li>
                <li class="text-secondary text-bold"><a class="fg-white" href="/webroot/Admin/Settings/Company_settings.aspx">Company settings</a></li>

            </ul>
        </li>

    </ul>

    <div class="app-bar-element place-right">
        <span id="welcome" runat="server" class="dropdown-toggle text-secondary text-bold"><span class="mif-user"></span></span>
        <div class="app-bar-drop-container padding10 place-right no-margin-top block-shadow fg-dark" data-role="dropdown" style="width: 220px;">

            <ul class="unstyled-list fg-dark">
                <li><a href="#" class="text-secondary fg-white text-bold fg-hover-yellow">Profile</a></li>
                <li><a href="#" class="text-secondary fg-white text-bold fg-hover-yellow">Security</a></li>
                <li><a href="/Login.aspx?msg=logoff" class="text-secondary fg-white text-bold fg-hover-yellow">Logout</a></li>
            </ul>
        </div>
    </div>

    <div id="notify" runat="server" visible="false" class="app-bar-element place-right">
        <a class=" fg-white"><span class="mif-bell"></span></a>
        <div style="width: 220px;" class="app-bar-drop-container bg-white fg-dark place-right"
            data-role="dropdown" data-no-close="true">
            <div class="padding10">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <ul class="unstyled-list fg-dark">
                            <li><a href="/Webroot/Admin/HelpDesk/Ticket_new.aspx?edit-id=<%#Eval("UserID")%>" class="fg-white1 text-secondary text-bold fg-hover-yellow"><%#Eval("UserID")%></a></li>
                            <li class="text-small"><a href="#" class="text-small"></a><b><%#Eval("SubmittedBy")%></b> <%#Eval("Description")%></li>
                            <hr height="1" />
                        </ul>
                    </ItemTemplate>
                </asp:Repeater>
                <asp:Label ID="Label1" CssClass="fg-red text-small" Visible="false" runat="server" Text=""></asp:Label>
                <asp:HyperLink ID="HyperLink1" CssClass="fg-lightBlue text-bold text-small" NavigateUrl="~/Webroot/Admin/HelpDesk/Activity.aspx" Text="View All" runat="server"></asp:HyperLink>
            </div>
        </div>
    </div>
    <span class="app-bar-pull"></span>

    <div class="app-bar-pullbutton automatic" style="display: none;"></div>
    <div class="clearfix" style="width: 0;"></div>
    <nav class="app-bar-pullmenu hidden flexstyle-app-bar-menu" style="display: none;">
        <ul class="app-bar-pullmenubar hidden app-bar-menu"></ul>
    </nav>
    <%--</div>--%>
</header>


