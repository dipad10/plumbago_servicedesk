﻿Imports DevExpress.Web
Partial Class Controls_Users
    Inherits System.Web.UI.UserControl
    Public Event SelectedItemChanged(ByVal newstudent As GHD5.User)
    Protected Sub combo1_ItemsRequestedByFilterCondition(source As Object, e As ListEditItemsRequestedByFilterConditionEventArgs)
        Dim data = (New cls_users).SelectAllFilter(e.Filter)
        Me.FillDropPartyIDGrid(data)
    End Sub

    Protected Sub combo1_ItemRequestedByValue(source As Object, e As ListEditItemRequestedByValueEventArgs)
        If Not String.IsNullOrEmpty(e.Value) Then
            Dim data = (New cls_users).SelectThisByRefuseridlist(e.Value)
            Me.FillDropPartyIDGrid(data)
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Me.IsPostBack = False Then
            Dim data = (New cls_users).SelectAllbasicusers()
            Me.FillDropPartyIDGrid(data)
        End If
    End Sub

    Private Sub FillDropPartyIDGrid(ByVal data As List(Of GHD5.User))
        With Me.combo1
            .DataSource = data
            .Columns.Clear()
            .Columns.Add("UserID").Width = 60
            .Columns.Add("Username").Width = 120
            .Columns.Add("Email").Width = 220
            .ValueField = "UserID"
            .TextField = "Username"
            .TextFormatString = "{1}"
            .DataBind()
        End With
    End Sub

    Public Property SelectedValue() As String
        Get
            Return Me.combo1.Value
        End Get
        Set(ByVal value As String)
            Me.combo1.Value = value
        End Set
    End Property

    Public Property SelectedText() As String
        Get
            Return Me.combo1.Text
        End Get
        Set(ByVal value As String)
            Me.combo1.Text = value
        End Set
    End Property
End Class
